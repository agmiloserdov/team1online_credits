﻿using System;
using System.Collections.Generic;
using System.Text;
using OnlineCreditsTeam1.Services;
using Xunit;

namespace OnlineCreditTests.Units
{
    public class PasswordGeneratorTest
    {
        [Fact]
        public void PasswordLengthTest()
        {
            PasswordGenerator generator = new PasswordGenerator();
            int expected = 5;
            string result = generator.GeneratePassword(expected);
            Assert.Equal(expected, result.Length);
        }

        [Fact]
        public void PasswordTypeTest()
        {
            int expected = 5;
            PasswordGenerator generator = new PasswordGenerator();
            Assert.True(generator.GeneratePassword(expected).GetType() == typeof(string));
        }
    }
}
