﻿using OnlineCreditsTeam1.Models.SettingsViewModels;
using OnlineCreditsTeam1.Services;
using Xunit;

namespace OnlineCreditTests.Units
{
    public class DataLoaderTest
    {
        [Fact]
        public void SaveFileTest()
        {
            DataLoader loader = new DataLoader("~/..", "file.json");
            SettingViewModel model = new SettingViewModel()
            {
                AutoCancelOrder = 15,
                FileSize = 10
            };

            loader.SerializeList(model);

           
            Assert.Equal(model.FileSize, loader.LoadDeserializedList().FileSize);
            Assert.Equal(model.AutoCancelOrder, loader.LoadDeserializedList().AutoCancelOrder);
        }
    }
}
