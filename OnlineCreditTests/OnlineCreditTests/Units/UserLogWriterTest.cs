﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Moq;
using OnlineCreditsTeam1.Models;
using OnlineCreditsTeam1.Services;
using Xunit;

namespace OnlineCreditTests.Units
{
    public class UserLogWriterTest
    {
        [Fact]
        public void GetAllLogsTest()
        {
            var mock = new Mock<IRepository>();
            mock.Setup(repo => repo.GetUserById("1")).Returns(GetUser());
            UserLogWriter logWriter = new UserLogWriter(mock.Object);
            List<LogModel> logs = new List<LogModel>();
            logs.Add(new LogModel { ActionDate = DateTime.Parse("0001-01-01T00:00:00.0000000"), LogMessage = "Demo message", UserId = "Demo Id" });
            string expectedPath = "wwwroot/logs/1.json";
            List<LogModel> expectedModel = logs;
            Assert.Equal(expectedModel.Count, logWriter.GetAllLogs("1").Count);
            Assert.Equal(expectedPath, GetUser().LogFilePath);
        }

        public ApplicationUser GetUser()
        {
            return new ApplicationUser
            {
                Name = "Test User", Id = "1", CreateTime = DateTime.Now, CreditAdminId = "2", Status = "Active",
                Role = "user", Email = "test@test.ru", PhoneNumber = "+996705014880",
                LogFilePath = $"wwwroot/logs/1.json"
            };
        }
    }
}
