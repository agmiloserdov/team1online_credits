﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using OnlineCreditsTeam1.Controllers;
using OnlineCreditsTeam1.Data;
using OnlineCreditsTeam1.Models;
using OnlineCreditsTeam1.Services;
using SQLitePCL;
using Xunit;

namespace OnlineCreditTests.Units
{
    public class ReportGeneratorTest
    {
        

        [Fact]
        public void GetReportLinkTest()
        {
            ReportGenerator reportGenerator = new ReportGenerator();
            List<Order> orders = new List<Order>();
            orders.Add(new Order(){BeginDate = DateTime.Now, ClientId = "clientTestId", FinishDate = DateTime.Now.AddDays(10), CorpClientId = "testCorpClient", Status = "одобрена", Sum = 15000, Id = 1, DesiredDay = DateTime.MaxValue, OrderInfoJson = "test", PercentRateDown = 2.2, Period = 12, UltimateSum = 100});
            orders.Add(new Order(){BeginDate = DateTime.Now, ClientId = "clientTestId1", FinishDate = DateTime.Now.AddDays(20), CorpClientId = "testCorpClient", Status = "одобрена", Sum = 1000, Id = 2, DesiredDay = DateTime.MaxValue, OrderInfoJson = "test", PercentRateDown = 2.2, Period = 12, UltimateSum = 100});

            string expected = $"~/Reports/{orders[0].CorpClientId}_report.csv";
            string reportLink = reportGenerator.GetReportLink(orders);
            Assert.Equal(reportLink,  expected);
        }

        [Fact]
        public void OrderListToCsvTest()
        {
            ReportGenerator reportGenerator = new ReportGenerator();
            List<Order> orders = new List<Order>();
            orders.Add(new Order() { BeginDate = DateTime.Now, ClientId = "clientTestId", FinishDate = DateTime.Now.AddDays(10), CorpClientId = "testCorpClient", Status = "одобрена", Sum = 15000, Id = 1, DesiredDay = DateTime.MaxValue, OrderInfoJson = "test", PercentRateDown = 2.2, Period = 12, UltimateSum = 100 });
            orders.Add(new Order() { BeginDate = DateTime.Now, ClientId = "clientTestId1", FinishDate = DateTime.Now.AddDays(20), CorpClientId = "testCorpClient", Status = "одобрена", Sum = 1000, Id = 2, DesiredDay = DateTime.MaxValue, OrderInfoJson = "test", PercentRateDown = 2.2, Period = 12, UltimateSum = 100 });

            string csvString = reportGenerator.ListToCsv(orders, ",");
            Assert.True(csvString != null);
        }

        [Fact]
        public void ReportModelListToCsvTest()
        {
            ReportGenerator reportGenerator = new ReportGenerator();
            List<ReportModel> reports = new List<ReportModel>();
            reports.Add(new ReportModel(){BeginDate = DateTime.Now, UserId = "TestId_1", Sum = 666, OrderStatus = "new"});
            reports.Add(new ReportModel(){BeginDate = DateTime.Now, UserId = "TestId_2", Sum = 777, OrderStatus = "new"});
            reports.Add(new ReportModel(){BeginDate = DateTime.Now, UserId = "TestId_2", Sum = 777, OrderStatus = "new"});
            string csvString = reportGenerator.ListToCsv(reports, ",");
            Assert.True(csvString != null && csvString is string);
        }

        [Fact]
        public void ReportModelCsvStringTest()
        {
            DateTime date = DateTime.Now;
            ReportGenerator reportGenerator = new ReportGenerator();
            List<ReportModel> reports = new List<ReportModel>();
            reports.Add(new ReportModel() { BeginDate = date, UserId = "TestId_1", Sum = 666, OrderStatus = "new" });
            reports.Add(new ReportModel() { BeginDate = date, UserId = "TestId_2", Sum = 777, OrderStatus = "new" });
            reports.Add(new ReportModel() { BeginDate = date, UserId = "TestId_2", Sum = 777, OrderStatus = "new" });
            string csvString = reportGenerator.ListToCsv(reports, ",");
            string expected = $"BeginDate,Sum,UserId,OrderStatus\n{date},666,TestId_1,new\n{date},777,TestId_2,new\n{date},777,TestId_2,new\n";
            Assert.Equal(expected, csvString);
        }
    }
}
