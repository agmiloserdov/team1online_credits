﻿using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using OnlineCreditsTeam1.Data;
using OnlineCreditsTeam1.Models;
using OnlineCreditsTeam1.Services;
using Xunit;

namespace OnlineCreditTests.Units
{
    public class UserActionLoggerTest
    {
        [Fact]
        public void GetLogsByIdTest()
        {
            var mock = new Mock<ILogRepository>();
            mock.Setup(repo => repo.GetLogsById("DemoId")).Returns(GetLogById());
            mock.Setup(repo => repo.GetAllLogs()).Returns(GetLogs());
            UserActionLogger logWriter = new UserActionLogger(mock.Object);
            List<UserActionLogModel> logs = new List<UserActionLogModel>();
            logs.Add(new UserActionLogModel
            {  Id = "1",
                Action = "test action",
                ActionDate = DateTime.Parse("0001-01-01T00:00:00.0000000"),
                UserId = "DemoId"});
            List<UserActionLogModel> expectedModel = logs;
            Assert.Equal(expectedModel.Count, logWriter.LoadUserLog("DemoId").Result.Logs.Count);

        }
        [Fact]
        public void GetAllLogsTest()
        {
            var mock = new Mock<ILogRepository>();
            mock.Setup(repo => repo.GetAllLogs()).Returns(GetLogs());
            UserActionLogger logWriter = new UserActionLogger(mock.Object);
            List<UserActionLogModel> expectedModel = new List<UserActionLogModel>();
            expectedModel.Add(new UserActionLogModel
            {
                Id = "1",
                Action = "test action",
                ActionDate = DateTime.Parse("0001-01-01T00:00:00.0000000"),
                UserId = "DemoId"
            });
            expectedModel.Add(new UserActionLogModel
            {
                Id = "2",
                Action = "test action 2",
                ActionDate = DateTime.Parse("0001-01-01T00:00:00.0000000"),
                UserId = "DemoId"
            });

            Assert.Equal(expectedModel.Count, logWriter.LoadUserLog().Result.Logs.Count);
        }
        public List<UserActionLogModel> GetLogById()
        {
            UserActionLogModel model = new UserActionLogModel
            {
                Id = "1",
                Action = "test action",
                ActionDate = DateTime.Parse("0001-01-01T00:00:00.0000000"),
                UserId = "DemoId"
            };
            List<UserActionLogModel> list = new List<UserActionLogModel>();
            list.Add(model);
            return list;
        }

        public List<UserActionLogModel> GetLogs()
        {
            List<UserActionLogModel> list = new List<UserActionLogModel>();
            list.Add(new UserActionLogModel
            {
                Id = "1",
                Action = "test action",
                ActionDate = DateTime.Parse("0001-01-01T00:00:00.0000000"),
                UserId = "DemoId"
            });
            list.Add(new UserActionLogModel
            {
                Id = "2",
                Action = "test action 2",
                ActionDate = DateTime.Parse("0001-01-01T00:00:00.0000000"),
                UserId = "DemoId"
            });
            return list;
        }
    }
}
