﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OnlineCreditsTeam1.Models.AccountViewModels;
using OnlineCreditsTeam1.Services;
using Xunit;

namespace OnlineCreditTests.Units
{
    public class JsonServiceTest
    {
        private readonly ILogger _logger;
        [Fact]
            public void CreateJsonTest()
            {

                JsonService jsonService = new JsonService();
                string result;
                string output = "";
                CreatureByTheAdminUserViewModel model = new CreatureByTheAdminUserViewModel()
                {
                    Role = "MarketOperator",
                    Bik = "123425646",
                    BankAccountNumber = "65757456",
                    LegalAdress = "hghgfdfgg"
                };
                string result1 ="[{\"Bik\":\"123425646\",\"BankAccountNumber\":\"65757456\",\"LegalAdress\":\"hghgfdfgg\"}]";
                result = jsonService.CreateJson(model);
                Assert.Equal(result1, result);
            }

        [Fact]
        public void GetJsonTest()
        {
            JsonService JsonService = new JsonService();
            RequisitViewModel model = new RequisitViewModel()
            {
                Bik = "123425646",
                BankAccountNumber = "65757456",
                LegalAdress = "hghgfdfgg"
            };
            string result1 = "[{\"Bik\":\"123425646\",\"BankAccountNumber\":\"65757456\",\"LegalAdress\":\"hghgfdfgg\"}]";

            RequisitViewModel modelOut = JsonService.GetJson(result1);
            Assert.Equal(model.Bik, modelOut.Bik);
            Assert.Equal(model.Inn, modelOut.Inn);
            Assert.Equal(model.LegalAdress, modelOut.LegalAdress);
            Assert.Equal(model.BankAccountNumber, modelOut.BankAccountNumber);
        }
        
    }
}
