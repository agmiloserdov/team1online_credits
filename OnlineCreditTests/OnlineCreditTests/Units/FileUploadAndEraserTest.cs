﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Moq;
using OnlineCreditsTeam1.Services;
using Xunit;

namespace OnlineCreditTests.Units
{
   
    public class FileUploadAndEraserTest
    {
        [Fact]
        public void Upload()
        {          
            FileUploadService fileUpload = new FileUploadService();

            var fileMock = new Mock<IFormFile>();        
            var content = "Yes, Betch";
            var fileName = "test.pdf";
            var ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
            fileMock.Setup(_ => _.OpenReadStream()).Returns(ms);
            fileMock.Setup(_ => _.FileName).Returns(fileName);
            fileMock.Setup(_ => _.Length).Returns(ms.Length);

            var file = fileMock.Object;

            fileUpload.Upload("/one", "test.pdf", file);
            Assert.True(File.Exists("/one/test.pdf"));
            
        }

        [Fact]
        public void DeleteFile()
        {
            FileEraser fileEraser = new FileEraser();
            fileEraser.DeleteFile("/one/test.pdf");
            Assert.False(File.Exists("/one/test.pdf"));
        }
    }
}
