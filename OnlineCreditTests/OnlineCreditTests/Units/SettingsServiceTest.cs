﻿using System;
using System.Collections.Generic;
using System.Text;
using OnlineCreditsTeam1.Models.SettingsViewModels;
using OnlineCreditsTeam1.Services;
using Xunit;

namespace OnlineCreditTests.Units
{
    public class SettingsServiceTest
    {
        [Fact]
        public void SaveSettings()
        {
            SettingsService service = new SettingsService();
            
            SettingViewModel model = new SettingViewModel()
            {
                AutoCancelOrder = 15,
                FileSize = 10
            };

            service.SaveSettings(model);
            
            Assert.Equal(model.FileSize, service.GetSettnigs().FileSize);
            Assert.Equal(model.AutoCancelOrder, service.GetSettnigs().AutoCancelOrder);

          
        }
    }
}
