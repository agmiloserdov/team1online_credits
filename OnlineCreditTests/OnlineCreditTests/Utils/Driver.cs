﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace OnlineCreditTests.Utils
{
    class Driver
    {
        private static IWebDriver driver;

        public static IWebDriver GetDriver(string driverPath)
        {
            if (driver == null)
            {
                driver = new ChromeDriver(driverPath);
            }

        return driver;
        }
    }
}
