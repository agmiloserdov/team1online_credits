﻿using System;
using System.Threading;
using OnlineCreditTests.Integration.StepDefinitions;
using Xunit;
using TechTalk.SpecFlow;

namespace OnlineCreditTests.Integration.StepDefinitions
{
    [Binding]
    public class GlobalSettingsSteps
    {
        private BasicSteps basicSteps;

        public GlobalSettingsSteps(BasicSteps basicSteps)
        {
            this.basicSteps = basicSteps;
        }
        [Given(@"я нахожусь на главной странице")]
        public void ДопустимЯНахожусьНаГлавнойСтранице()
        {
            basicSteps.GoToMainPage();
        }
        

        [Given(@"я админ")]
        public void ДопустимЯАвторизованныйПользователь()
        {

            basicSteps.SelectElement("Номер телефона", "Кыргызстан +996");
            basicSteps.ClickInput("phone");
            basicSteps.FillText("phone", "123456789"); 
            basicSteps.ClickButton("Отправить пароль");
            basicSteps.ClickButton("Вход");
        }
        
        [Given(@"нажимаю на вкладку ""(.*)""")]
        public void ДопустимНажимаюНаВкладку(string link)
        {
            basicSteps.ClickLink("Admin");
            basicSteps.ClickLink(link);
        }

        [Then(@"вижу страницу с настройками сайта")]

        public void ТоВижуСтраницуСНастройкамиСайта()
        {
            basicSteps.IsElementFound("Admin");
            basicSteps.IsElementFound("Подробная информация");
            Thread.Sleep(1000);
            basicSteps.IsElementFound("Глобальные настройки");
        }

        [Then(@"изменяю настройку Размер файла")]

        public void ТоИзменяюНастройкуРазмерФайла()
        {
            basicSteps.DeleteText("fail_length");
            basicSteps.FillText("fail_length", "1000");

        }
        [When(@"нажимаю ""(.*)""")]

        public void ЕслиНажимаю(string link)

        {
            basicSteps.ClickButton(link);
        }

        [Then(@"настройки сохраняются")]
        public void ТоНатсройкиСохраняются()
        {
            basicSteps.ClickLink("Настройки");
            string value = basicSteps.GetValueInput("fail_length");
            Assert.True(value =="1000");
        }

        [AfterTestRun]
        public static void Close()
        {
            BasicSteps.driver.Quit();
            BasicSteps.driver.Close();
        }
    }
}
