﻿using System;
using System.Threading;
using TechTalk.SpecFlow;
using Xunit;

namespace OnlineCreditTests.Integration.StepDefinitions
{
    [Binding]
    public class ValidationCreateRateSteps
    {
        private BasicSteps basicSteps;

        public ValidationCreateRateSteps(BasicSteps basicSteps)
        {
            this.basicSteps = basicSteps;
        }
        [Given(@"я нахожусь на странице процентных ставок")]
        public void ДопустимЯНахожусьНаСтраницеПроцентныхСтавок()
        {
            basicSteps.GoToMainPage();

            basicSteps.SelectElement("Номер телефона", "Кыргызстан +996");
            basicSteps.ClickInput("phone");
            basicSteps.FillText("phone", "123456789");
            basicSteps.ClickButton("Отправить пароль");
            basicSteps.ClickButton("Вход");
            basicSteps.ClickLink("Admin");
            basicSteps.ClickLink("Процентные ставки");
        }
        
        [Given(@"я администратор")]
        public void ДопустимЯАдминистратор()
        {
            basicSteps.IsElementFound("Admin");
            Thread.Sleep(1000);
        }
        
        [Given(@"я нажимаю на ссылку ""(.*)""")]
        public void ДопустимЯНажимаюНаСсылку(string link)
        {
            
            basicSteps.ClickLink(link);
        }
        
              
        [Then(@"вижу форму создания процентной ставки")]
        public void ТоВижуФормуСозданияПроцентнойСтавки()
        {
            basicSteps.IsElementFound("Код валюты");
            basicSteps.IsElementFound("Сумма до:");
        }
        

        [Then(@"я заполняю необходимые поля некорректными данными")]
        public void ТоЯЗаполняюНеобходимыеПоля()
        {
            basicSteps.FillText("BeginDate1", "08.02.2018");
            basicSteps.FillText("FinishDate1", "10.02.2021");
            basicSteps.FillText("CreditTermFrom1", "5");
            basicSteps.FillText("CreditTermTo1", "12");
            basicSteps.FillText("TotalSumFrom1", "1000");
            basicSteps.FillText("TotalSumTo1", "10000");
            basicSteps.FillText("PercentRate1", "6");
            
        }

        [When(@"я нажимаю ""(.*)""")]
        public void ЕслиЯНажимаю(string btn)
        {
            basicSteps.ClickButton(btn);
        }

        [Then(@"вижу ошибку валидации")]
        public void ТоВижуОшибкуВалидации()

        {
            basicSteps.IsElementFound("Ошибка! Не корректно введенные данные");
        }

        [AfterTestRun]
        public static void Close()
        {
            BasicSteps.driver.Quit();
            BasicSteps.driver.Close();
        }
    }
}
