﻿using System;
using OnlineCreditTests.Utils;
using TechTalk.SpecFlow;

namespace OnlineCreditTests.Integration.StepDefinitions
{
    [Binding]
    public class CheckErrorPageSteps
    {
        private BasicSteps basicSteps;
        public static IWebDriver driver;
        private readonly string DRIVER_PATH = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        public CheckErrorPageSteps(BasicSteps basicSteps)
        {
            this.basicSteps = basicSteps;
            this.driver = Driver.GetDriver(DRIVER_PATH);
        }
        [Given(@"я вхожу в личный кабинет и я администратор")]
        public void ДопустимЯВхожуВЛичныйКабинетИЯАдминистратор()
        {
            basicSteps.GoToMainPage();
        }
        
        [When(@"я перехожу по несуществующему ""(.*)""")]
        public void ЕслиЯПерехожуПоНесуществующему(string p0)
        {
            driver.Navigate().GoToUrl("/asasasaa");
        }
        
        [Then(@"я вижу страницу с сообщением ""(.*)""")]
        public void ТоЯВижуСтраницуССообщением(int p0)
        {
            basicSteps.IsElementFound("404 – страница не найдена");
        }
    }
}
