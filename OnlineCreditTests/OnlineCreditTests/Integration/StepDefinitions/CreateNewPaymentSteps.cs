﻿using System;
using System.Threading;
using OnlineCreditTests.Integration.StepDefinitions;
using OpenQA.Selenium.Internal;
using TechTalk.SpecFlow;

namespace OnlineCreditTests
{
    [Binding]
    public class CreateNewPaymentSteps
    {
        private BasicSteps basicSteps;

        public CreateNewPaymentSteps(BasicSteps basicSteps)
        {
            this.basicSteps = basicSteps;
        }
        [Given(@"я нахожусь на странице платежей")]
        public void ДопустимЯНахожусьНаСтраницеПлатежей()
        {
            basicSteps.GoToMainPage();

            basicSteps.SelectElement("Номер телефона", "Кыргызстан +996");
            basicSteps.ClickInput("phone");
            basicSteps.FillText("phone", "123456789");
            basicSteps.ClickButton("Отправить пароль");
            basicSteps.ClickButton("Вход");
            basicSteps.ClickLink("Admin");
            
            Thread.Sleep(1000);
        }
        
        [Given(@"я являюсь  администратором")]
        public void ДопустимЯЯвляюсьАдминистратором()
        {
            basicSteps.ClickLink("Виды платежей");
            Thread.Sleep(1000);
        }
        
        [Given(@"я нажимаю  на  ссылку ""(.*)""")]
        public void ДопустимЯНажимаюНаСсылку(string link)
        {
            Thread.Sleep(1000);
            basicSteps.ClickLink(link);
            Thread.Sleep(1000);
        }

        [Then(@"вижу форму создания нового платежа")]
        public void ТоВижуФормуСозданияНовогоПлатежа()
        {
            basicSteps.IsElementFound("Тип платежа");
            basicSteps.IsElementFound("Срок в месяцах");
        }

        [Then(@"я заполняю необходимые поля коректными  данными")]
        public void ТоЯЗаполняюНеобходимыеПоляКоректнымиДанными()
        {
            basicSteps.FillText("Name", "Test");
            basicSteps.FillText("Term", "3");
            Thread.Sleep(1000);
        }

        [Given(@"я нажимаю   кнопку ""(.*)""")]
        public void ДопустимЯНажимаюКнопку(string btn)
        {
            Thread.Sleep(1000);
            basicSteps.ClickLink(btn);
        }

        [Then(@"я  вижу страницу личного кабинета администратора")]
        public void ТоЯВижуСтраницуЛичногоКабинетаАдминистратора()
        {
            basicSteps.IsElementFound("Admin");
        }
        [Then(@"я нажимаю   ""(.*)""")]
        public void ТоЯНажимаю(string btn)
        {
            Thread.Sleep(1000);
            basicSteps.ClickButton(btn);
        }

        [Given(@"вижу новый  платеж в списке платежей")]
        public void ДопустимВижуНовыйПлатежВСпискеПлатежей()
        {
            basicSteps.IsElementFound("Test");
        }
        [AfterTestRun]
        public static void Close()
        {
            BasicSteps.driver.Quit();
            BasicSteps.driver.Close();
        }







    }
}
