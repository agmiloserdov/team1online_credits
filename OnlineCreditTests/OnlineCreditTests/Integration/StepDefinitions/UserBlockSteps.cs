﻿using System;
using System.Threading;
using OnlineCreditTests.Integration.StepDefinitions;
using TechTalk.SpecFlow;

namespace OnlineCreditTests
{
    [Binding]
    public class UserBlockSteps
    {
        private BasicSteps basicSteps;

        public UserBlockSteps(BasicSteps basicSteps)
        {
            this.basicSteps = basicSteps;
        }
        [Given(@"я нахожусь на странице пользователей")]
        public void ДопустимЯНахожусьНаСтраницеПользователей()
        {
            basicSteps.GoToMainPage();
            basicSteps.SelectElement("Номер телефона", "Кыргызстан +996");
            basicSteps.ClickInput("phone");
            basicSteps.FillText("phone", "123456789");
            basicSteps.ClickButton("Отправить пароль");
            basicSteps.ClickButton("Вход");
            basicSteps.ClickLink("Admin");
            basicSteps.ClickLink("Пользователи");
        }
        
        [Given(@"я  администратор")]
        public void ДопустимЯАдминистратор()
        {
            basicSteps.IsElementFound("Admin");
            Thread.Sleep(1000);
        }
        
        [Given(@"я нажимаю на ссылку  ""(.*)""")]
        public void ДопустимЯНажимаюНаСсылку(string link)
        {

            basicSteps.ClickLinkByInside(link);
        }
        
        [Then(@"я вижу его личный кабинет")]
        public void ТоЯВижуЕгоЛичныйКабинет()
        {
            basicSteps.IsElementFound("Имя :");
        }
        
        [Then(@"я нажимаю на кнопку ""(.*)""")]
        public void ТоЯНажимаюНаКнопку(string btn)
        {
            basicSteps.ClickButton(btn);
        }
        
        [Then(@"вижу сообщение что пользователь заблокирован")]
        public void ТоВижуСообщениеЧтоПользовательЗаблокирован()
        {
            basicSteps.IsElementFound("Пользователь заблокирован");
        }
       
        [Then(@"я нажимаю на ссылку ""(.*)""")]
        public void ТоЯНажимаюНаСсылку(string link)
        {
            basicSteps.ClickLink(link);
        }
 
        [Then(@"я жму на кнопку ""(.*)""")]
        public void ТоЯЖмуНаКнопку(string btn)
        {
            basicSteps.ClickButton(btn);
        }

        [Then(@"вижу сообщение о том что пользователь разблокирован")]
        public void ТоВижуСообщениеОТомЧтоПользовательРазблокирован()
        {
            basicSteps.IsElementFound("Пользователь разблокирован");
        }
        [AfterTestRun]
        public static void Close()
        {
            BasicSteps.driver.Quit();
            BasicSteps.driver.Close();
        }
    }
}
