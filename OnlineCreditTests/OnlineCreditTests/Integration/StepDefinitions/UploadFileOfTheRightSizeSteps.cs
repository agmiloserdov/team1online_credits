﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using Microsoft.SqlServer.Server;
using Xunit;
using TechTalk.SpecFlow;


namespace OnlineCreditTests.Integration.StepDefinitions
{
    [Binding]
    public class UploadFileOfTheRightSizeSteps
    {

        private BasicSteps basicSteps;

        public UploadFileOfTheRightSizeSteps(BasicSteps basicSteps)
        {
            this.basicSteps = basicSteps;
        }

        [Given(@"я захожу под админом")]
        public void ДопустимЯЗахожуПодАдмином()
        {
            basicSteps.GoToMainPage();

            basicSteps.SelectElement("Номер телефона", "Кыргызстан +996");
            basicSteps.ClickInput("phone");
            basicSteps.FillText("phone", "123456789");
            basicSteps.ClickButton("Отправить пароль");
            basicSteps.ClickButton("Вход");
        }
        
        [Given(@"оказываюсь в личном кабинете админа")]
        public void ДопустимОказываюсьВЛичномКабинетеАдмина()
        {
            Thread.Sleep(1000);
            basicSteps.IsElementFound("Admin");
        }
        
        [When(@"я выбираю файл явно большего размера, чем установелно системой, чтоб поменять ему аватарку")]
        public void ЕслиЯВыбираюФайлЯвноБольшегоРазмераЧемУстановелноСистемойЧтобПоменятьЕмуАватарку()
        {
            string absolutePath = AppDomain.CurrentDomain.BaseDirectory;
            string path = "";
            List<string> l = new List<string>();
            foreach (string s in absolutePath.Split('\\'))
            {
                l.Add(s);
            }

            for (int i = 0; i < l.Count - 3; i++)
            {
                path += l[i] + "\\";
            }

            path += "111.png";
            basicSteps.UploadFile("uploadImage", path);
        }
        
        [When(@"нажимаю кнопку ""(.*)""")]
        public void ЕслиНажимаюКнопку(string btn)
        {
            basicSteps.ClickButtonById("saveImage");
            Thread.Sleep(2000);
        }
        
        [Then(@"вижу сообщение что файл больше, чем возможно")]
        public void ТоВижуСообщениеЧтоФайлБольшеЧемВозможно()
        {
            basicSteps.IsElementFound("Размер файла больше допустимого");
        }

        [AfterTestRun]
        public static void Close()
        {
            BasicSteps.driver.Quit();
            BasicSteps.driver.Close();
        }

    }
}
