﻿using System;
using System.Threading;
using OnlineCreditTests.Integration.StepDefinitions;
using TechTalk.SpecFlow;

namespace OnlineCreditTests
{
    [Binding]
    public class ChangeNameSteps
    {
        private BasicSteps basicSteps;

        public ChangeNameSteps(BasicSteps basicSteps)
        {
            this.basicSteps = basicSteps;
        }
        [Given(@"я нахожусь на странице  пользователей")]
        public void ДопустимЯНахожусьНаСтраницеПользователей()
        {
            basicSteps.GoToMainPage();
            basicSteps.SelectElement("Номер телефона", "Кыргызстан +996");
            basicSteps.ClickInput("phone");
            basicSteps.FillText("phone", "123456789");
            basicSteps.ClickButton("Отправить пароль");
            basicSteps.ClickButton("Вход");
            basicSteps.ClickLink("Admin");
            basicSteps.ClickLink("Пользователи");
        }
        
        [Given(@"я   администратор")]
        public void ДопустимЯАдминистратор()
        {
            basicSteps.IsElementFound("Admin");
            Thread.Sleep(1000);
        }
        
        [Given(@"я нажимаю  на ссылку  ""(.*)""")]
        public void ДопустимЯНажимаюНаСсылку(string link)
        {
            Thread.Sleep(1000);
            basicSteps.ClickLinkByInside(link);
        }
        
        [Then(@"я вижу  его личный кабинет")]
        public void ТоЯВижуЕгоЛичныйКабинет()
        {
            basicSteps.IsElementFound("Имя :");
            Thread.Sleep(1000);
        }
        
        [Then(@"я нажимаю на инпут ""(.*)"" и удаляю содержимое")]
        public void ТоЯНажимаюНаИнпутИУдаляюСодержимое(string nd)
        {
            
            basicSteps.DeleteText("Name");
        }
        
        [Then(@"Ввожу новое имя ""(.*)""")]
        public void ТоВвожуНовоеИмя(string p0)
        {
            Thread.Sleep(1000);
            basicSteps.FillText("Name", "Roma");
        }
        
        [Then(@"я  нажимаю на кнопку ""(.*)""")]
        public void ТоЯНажимаюНаКнопку(string btn)
        {
            Thread.Sleep(1000);
            basicSteps.ClickButton(btn);
        }
        
        [Then(@"вижу сообщение ""(.*)""")]
        public void ТоВижуСообщение(string p0)
        {
            Thread.Sleep(1000);
            basicSteps.IsElementFound("Успех. Профиль был обновлен");
        }
    }
}
