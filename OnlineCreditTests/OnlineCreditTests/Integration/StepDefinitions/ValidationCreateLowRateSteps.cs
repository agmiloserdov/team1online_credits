﻿using System;
using System.Threading;
using TechTalk.SpecFlow;
using Xunit;


namespace OnlineCreditTests.Integration.StepDefinitions
{
    [Binding]
    public class ValidationCreateLowRateSteps
    {
        private BasicSteps basicSteps;

        public ValidationCreateLowRateSteps(BasicSteps basicSteps)
        {
            this.basicSteps = basicSteps;
        }
        [Given(@"я нахожусь на странице пониженных процентных ставок")]
        public void ДопустимЯНахожусьНаСтраницеПониженныхПроцентныхСтавок()
        {
            basicSteps.GoToMainPage();

            basicSteps.SelectElement("Номер телефона", "Кыргызстан +996");
            basicSteps.ClickInput("phone");
            basicSteps.FillText("phone", "123456789");
            basicSteps.ClickButton("Отправить пароль");
            basicSteps.ClickButton("Вход");
            basicSteps.ClickLink("Admin");
            basicSteps.ClickLink("Процентные ставки");
        }
        
        [Given(@"я являюсь администратором")]
        public void ДопустимЯЯвляюсьАдминистратором()
        {
            basicSteps.IsElementFound("Admin");
            Thread.Sleep(1000);
        }
        
        [Given(@"я нажимаю на ""(.*)""")]
        public void ДопустимЯНажимаюНа(string link)
        {
            basicSteps.ClickLink(link);
        }
        
        [Then(@"вижу форму создания пониженной процентной ставки")]
        public void ТоВижуФормуСозданияПониженнойПроцентнойСтавки()
        {
            basicSteps.IsElementFound("Код валюты");
            basicSteps.IsElementFound("Сумма до:");
        }
        
        [Then(@"я заполняю необходимые поля неправильными данными")]
        public void ТоЯЗаполняюНеобходимыеПоляНеправильнымиДанными()
        {
            basicSteps.FillText("BeginDate2", "08.02.2018");
            basicSteps.FillText("FinishDate2", "10.02.2021");
            basicSteps.FillText("CreditTermFrom2", "5");
            basicSteps.FillText("CreditTermTo2", "12");
            basicSteps.FillText("TotalSumFrom2", "1000");
            basicSteps.FillText("TotalSumTo2", "10000");
            basicSteps.FillText("PercentRate2", "6");
        }
        
        [Then(@"я нажимаю  ""(.*)""")]
        public void ТоЯНажимаю(string btn)
        {
            Thread.Sleep(1000);
            Thread.Sleep(1000);
            basicSteps.ClickButton(btn);
        }
        
        [Then(@"вижу ошибку валидации создания пониженной процентной ставки")]
        public void ТоВижуОшибкуВалидацииСозданияПониженнойПроцентнойСтавки()
        {
            basicSteps.IsElementFound("Ошибка! Не корректно введенные данные");
        }

        [AfterTestRun]
        public static void Close()
        {
            BasicSteps.driver.Quit();
            BasicSteps.driver.Close();
        }
    }
}
