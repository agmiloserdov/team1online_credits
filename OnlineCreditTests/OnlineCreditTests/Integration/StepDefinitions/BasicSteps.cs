﻿using System.IO;
using System.Reflection;
using OnlineCreditTests.Utils;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;
using Xunit;

[assembly: CollectionBehavior(DisableTestParallelization = true)]

namespace OnlineCreditTests.Integration.StepDefinitions
{
    [Binding]
    public class BasicSteps
    {
        public static IWebDriver driver;
        private readonly string DRIVER_PATH = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        private const string MANE_PAGE_URL = "http://35.237.152.69:80/";

        public BasicSteps()
        {
            driver = Driver.GetDriver(DRIVER_PATH);
        }

        public void GoToMainPage()                          // главная страница, откуда все и начинается
        {
            driver.Navigate().GoToUrl(MANE_PAGE_URL);
        }

        public void ClickLink(string linkText)              //нахождение ссылки по тексту и переход по ней
        {
            var link = driver.FindElement(By.LinkText(linkText));
            link.Click();
        }

        public void ClickButton(string caption)             //нахождение кнопки по тексту и нажатие на нее
        {
            var button = driver.FindElement(By.XPath($"//button[contains(text(),'{caption}')]"));
            button.Click();
        }

        public void ClickButtonById(string id)             //нахождение кнопки по id и нажатие на нее
        {
            var button = driver.FindElement(By.Id(id));
            button.Click();
        }

        public void FillText(string id, string text)        //нахождение элемента по Id и заполнение какимто текстом(для инпутов)
        {
            var textBox = driver.FindElement(By.Id(id));
            textBox.SendKeys(text);
        }

        public void FillTextNearLabel(string labelText, string text, string type)       //нахождение по названию label-а, ну и переходи к его инпуту и заполнение тектом
        {
            var textBox = driver.FindElement(By.XPath($"//label[contains(text(), '{labelText}')]/../input[@type='{type}']"));
            textBox.SendKeys(text);
        }

        public bool IsElementFound(string text)             //для проверки есть ли на страницы элемент с определенным именем
        {
            var element = driver.FindElement(By.XPath($"//*[contains(text(),'{text}')]"));
            return element != null;
        }

        public void SelectElement(string labelText, string element)    //нахождение по названию label-а, переход к селекту и выборка определенного options-а
        {
            var select = driver.FindElement(By.XPath($"//label[contains(text(), '{labelText}')]/../select"));
            var selectElement = new SelectElement(select);
            selectElement.SelectByText(element);
        }

        public string GetValueText(string id)               //нахождение по Id и получение содержимого (НЕ содержимое инпута)(актуально для нашего генерирующегося пароля)
        {
            var element = driver.FindElement(By.Id(id));
            return element.Text;
        }

        public void ClickInput(string id)        //нахождение элемента по Id и нажатие на него
        {
            var textBox = driver.FindElement(By.Id(id));
            textBox.Click();
        }

        public void DeleteText(string id)        //нахождение элемента по Id и очистка содержимого
        {
            var textBox = driver.FindElement(By.Id(id));
            textBox.Clear();
        }

        public string GetValueInput(string id)        //нахождение по Id и получение содержимого (для содержимого инпута)
        {
            var element = driver.FindElement(By.Id(id));
            return element.GetAttribute("value");
        }

        public void UploadFile( string id, string path)        //загрузка файла в элемент с айдишником по определенному путю
        {
            driver.FindElement(By.Id(id)).SendKeys(path);
        }
        public void ClickById(string id)             //нахождение элемента по id и нажатие на него
        {
            var button = driver.FindElement(By.Id(id));
            button.Click();
        }
        public void ClickLinkByInside(string Text)              //нахождение ссылки по тексту и переход по ней
        {
            var link = driver.FindElement(By.XPath($"//span[contains(text(),'{Text}')]/../../a"));
            link.Click();
        }
    }
}
