﻿using System;
using System.Threading;
using TechTalk.SpecFlow;

namespace OnlineCreditTests.Integration.StepDefinitions
{

    [Binding]
    public class ValidateLimitCreateSteps
    {
        private BasicSteps basicSteps;

        public ValidateLimitCreateSteps(BasicSteps basicSteps)
        {
            this.basicSteps = basicSteps;
        }
        [Given(@"я  админ")]
        public void ДопустимЯАдмин()
        {
            basicSteps.GoToMainPage();

            basicSteps.SelectElement("Номер телефона", "Кыргызстан +996");
            basicSteps.ClickInput("phone");
            basicSteps.FillText("phone", "123456789");
            basicSteps.ClickButton("Отправить пароль");
            basicSteps.ClickButton("Вход");
            basicSteps.ClickLink("Admin");
            basicSteps.ClickLink("Лимиты");
            Thread.Sleep(2000);
            basicSteps.ClickButton("Создать лимит");
        }
        
        [Given(@"я нахожусь на странице создания лимита")]
        public void ДопустимЯНахожусьНаСтраницеСозданияЛимита()
        {
            Thread.Sleep(1000);
            basicSteps.IsElementFound("Максимальная сумма кредитов за период");
        }
        
        [When(@"я ввожу в поле максимальный срок кредита большее число чем возможно")]
        public void ЕслиЯВвожуВПолеМаксимальныйСрокКредитаБольшееЧислоЧемВозможно()
        {
            Thread.Sleep(1000);
            basicSteps.FillText("limitSprint", "55");
            
        }
        [When(@"нажимаю кнопку создать")]

        public void ЕслиНажимаюКнопкуСоздать()
        {
            basicSteps.ClickById("btnCreateLimit");
        }

        [Then(@"вижу ошибку что введенное значение больше допустимого")]
        public void ТоВижуОшибкуЧтоВведенноеЗначениеБольшеДопустимого()
        {
            basicSteps.IsElementFound("Максимальная сумма кредитов за период");
        }
        [AfterTestRun]
        public static void Close()
        {
            BasicSteps.driver.Quit();
            BasicSteps.driver.Close();
        }
    }
}
