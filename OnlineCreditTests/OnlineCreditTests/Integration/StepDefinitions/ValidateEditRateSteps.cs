﻿using System;
using System.Threading;
using OnlineCreditTests.Integration.StepDefinitions;
using TechTalk.SpecFlow;
 
namespace OnlineCreditTests
{
    [Binding]
    public class ValidateEditRateSteps
    {
        private BasicSteps basicSteps;

        public ValidateEditRateSteps(BasicSteps basicSteps)
        {
            this.basicSteps = basicSteps;
        }
        [Given(@"я под админом нахожусь в лмчном кабинете")]
        public void ДопустимЯПодАдминомНахожусьВЛмчномКабинете()
        {
            basicSteps.GoToMainPage();

            basicSteps.SelectElement("Номер телефона", "Кыргызстан +996");
            basicSteps.ClickInput("phone");
            basicSteps.FillText("phone", "123456789");
            basicSteps.ClickButton("Отправить пароль");
            basicSteps.ClickButton("Вход");
        }
        
        [Given(@"перехожу по ссылке ""(.*)""")]
        public void ДопустимПерехожуПоСсылке(string link)
        {
            basicSteps.ClickLink(link);
            Thread.Sleep(1000);
        }

        [Given(@"еще перехожу по ссылке ""(.*)""")]
        public void ДопустимЕщеПерехожуПоСсылке(string link2)
        {
            basicSteps.ClickLink(link2);
        }

        [When(@"я ввожу в поле Срок кредита от: отрицательное число")]
        public void ЕслиЯВвожуВПолеСрокКредитаОтОтрицательноеЧисло()
        {
            basicSteps.DeleteText("creditTermFromTest");
            basicSteps.FillText("creditTermFromTest", "-1");
        }

        [When(@"нажимаю изменить")]

        public void ЕслиНажимаюИзменить()
        {
            basicSteps.ClickButton("Изменить");
        }

        [Then(@"вижу сообщение Недопустимое колличество месяцев")]
        public void ТоВижуСообщениеНедопустимоеКолличествоМесяцев()
        {
            basicSteps.IsElementFound("Недопустимое количество месяцев");
        }
        [AfterTestRun]
        public static void Close()
        {
            BasicSteps.driver.Quit();
            BasicSteps.driver.Close();
        }
    }
}
