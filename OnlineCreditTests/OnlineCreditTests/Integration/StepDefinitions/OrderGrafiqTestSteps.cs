﻿using System;
using OnlineCreditTests.Integration.StepDefinitions;
using TechTalk.SpecFlow;
using Xunit;


namespace OnlineCreditTests.Integration.StepDefinitions
{
    [Binding]
    public class OrderGrafiqTestSteps
    {
        private BasicSteps basicSteps;

        public OrderGrafiqTestSteps(BasicSteps basicSteps)
        {
            this.basicSteps = basicSteps;
        }

        [Given(@"я перехожу во вкладку заявки ""(.*)""")]
        public void ДопустимЯПерехожуВоВкладкуЗаявки(string p0)
        {
            basicSteps.SelectElement("Номер телефона", "Кыргызстан +996");
            basicSteps.ClickInput("phone");
            basicSteps.FillText("phone", "123456789");
            basicSteps.ClickButton("Отправить пароль");
            basicSteps.ClickButton("Вход");
            basicSteps.ClickButton(p0);
        }
        
        [When(@"статус заявки ""(.*)"" или ""(.*)""")]
        public void ЕслиСтатусЗаявкиИли(string p0, string p1)
        {
            basicSteps.IsElementFound(p0);
            basicSteps.IsElementFound(p1);
        }
        
        [Then(@"я вижу список поданных заявок")]
        public void ТоЯВижуСписокПоданныхЗаявок()
        {
            basicSteps.IsElementFound("Идентефикатор");
            basicSteps.IsElementFound("Сумма до:");
        }
        
        [Then(@"я открываю одну из заявок")]
        public void ТоЯОткрываюОднуИзЗаявок()
        {
            basicSteps.ClickButton("Подтверждено банком");
        }
        
        [Then(@"я вижу ""(.*)""")]
        public void ТоЯВижу(string p0)
        {
            basicSteps.IsElementFound(p0);
        }

        [AfterTestRun]
        public static void Close()
        {
            BasicSteps.driver.Quit();
            BasicSteps.driver.Close();
        }
    }
}
