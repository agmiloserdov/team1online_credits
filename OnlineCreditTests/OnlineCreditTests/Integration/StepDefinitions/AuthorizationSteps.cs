﻿using System;
using Xunit;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SpecFlow.GeneratedTests.Integration;
using TechTalk.SpecFlow;

namespace OnlineCreditTests.Integration.StepDefinitions
{
    
    [Binding]
    public class AuthorizationSteps
    {
        private Random run = new Random();
        private BasicSteps basicSteps;
        public AuthorizationSteps(BasicSteps basicSteps)
        {
            this.basicSteps = basicSteps;
        }

        [Given(@"я нахожусь на главной страниц")]
        public void ДопустимЯНахожусьНаГлавнойСтраниц()
        {
            basicSteps.GoToMainPage();
        }
        
        [Given(@"я анонимный пользователь")]
        public void ДопустимЯАнонимныйПользователь()
        {
            basicSteps.IsElementFound("Вход");
            basicSteps.IsElementFound("Регистрация");
        }
        
        [Given(@"я прохожу по ссылке ""(.*)""")]
        public void ЕслиЯПрохожуПоСсылке(string link)
        {
            basicSteps.ClickLink(link);
        }
        
        [Then(@"вижу страницу регистрации")]
        public void ТоВижуСтраницуРегистрации()
        {
            basicSteps.IsElementFound("Регистрация");
        }

        [Given(@"я нахожусь на странице регистрации")]
        public void ДопустимЯНахожусьНаСтраницеРегистрации()
        {
            ДопустимЯНахожусьНаГлавнойСтраниц();
            ЕслиЯПрохожуПоСсылке("Регистрация");
        }
        [Given(@"я заполняю регистрационные данные")]

        public void ДопустимЯЗаполняюРегистрационныеДанные()
        {
            basicSteps.FillTextNearLabel("Имя", "Roma", "text");
            basicSteps.SelectElement("Номер телефона", "Кыргызстан +996");
            basicSteps.ClickInput("phone");
            int number = run.Next(100000000, 999999999);
            basicSteps.FillText("phone", number.ToString());
            basicSteps.ClickButton("Регистрация");
        }

        [Then(@"я вижу страницу входа")]
        public void ТоЯВижуСтраницуВхода()
        {
            Assert.True(basicSteps.IsElementFound("Вход"));
        }

        [When(@"я ввожу пороль")]
        public void ЕслиЯВвожуПороль()
        {
            basicSteps.ClickButton("Вход");
        }

        [Then(@"я авторизуюсь")]
        public void ТоЯАвторизуюсь()
        {
            Assert.True(basicSteps.IsElementFound("Выход"));
        }

        [AfterTestRun]
        public static void Close()
        {
            BasicSteps.driver.Quit();
            BasicSteps.driver.Close();
        }
    }
}
