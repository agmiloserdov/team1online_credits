﻿using System;
using OnlineCreditTests.Integration.StepDefinitions;
using TechTalk.SpecFlow;

namespace OnlineCreditTests.Integration
{
    [Binding]
    public class ProfileButtonsRateTestSteps
    {
        private BasicSteps basicSteps;

        public ProfileButtonsRateTestSteps(BasicSteps basicSteps)
        {
            this.basicSteps = basicSteps;
        }
        [Given(@"Я вхожу в личный кабинет и я пользователь с ролью администратор")]
        public void ДопустимЯВхожуВЛичныйКабинетИЯПользовательСРольюАдминистратор()
        {
            basicSteps.GoToMainPage();
            basicSteps.SelectElement("Номер телефона", "Кыргызстан +996");
            basicSteps.ClickInput("phone");
            basicSteps.FillText("phone", "123456789");
            basicSteps.ClickButton("Отправить пароль");
            basicSteps.ClickButton("Вход");
        }
        
        [When(@"Я нажимаю кнопку ""(.*)""")]
        public void ЕслиЯНажимаюКнопку(string p0)
        {
            basicSteps.ClickLink(p0);
        }
        
        [Then(@"Вижу страницу с процентными ставками")]
        public void ТоВижуСтраницуСПроцентнымиСтавками()
        {
            basicSteps.IsElementFound("Процентные ставки");
        }
        
        [Then(@"если я нажимаю на кнопку ""(.*)""")]
        public void ТоЕслиЯНажимаюНаКнопку(string p0)
        {
            basicSteps.ClickLink(p0);
        }
        
        [Then(@"вижу список пониженных процентных ставок")]
        public void ТоВижуСписокПониженныхПроцентныхСтавок()
        {
            basicSteps.IsElementFound("Пониженные процентные ставки");
        }

        [AfterTestRun]
        public static void Close()
        {
            BasicSteps.driver.Quit();
            BasicSteps.driver.Close();
        }
    }
}
