﻿using System;
using System.Threading;
using TechTalk.SpecFlow;

namespace OnlineCreditTests.Integration.StepDefinitions
{
    [Binding]
    public class CreateNewUserSteps
    {
        private BasicSteps basicSteps;
        Random run = new Random();

        public CreateNewUserSteps(BasicSteps basicSteps)
        {
            this.basicSteps = basicSteps;
        }
        [Given(@"я вхожу в личный кабинет и я являюсь администратором")]
        public void ДопустимЯВхожуВЛичныйКабинетИЯЯвляюсьАдминистратором()
        {
            basicSteps.GoToMainPage();

            basicSteps.SelectElement("Номер телефона", "Кыргызстан +996");
            basicSteps.ClickInput("phone");
            basicSteps.FillText("phone", "123456789");
            basicSteps.ClickButton("Отправить пароль");
            basicSteps.ClickButton("Вход");
        }
        
        [Given(@"я нажимаю на кнопку ""(.*)""")]
        public void ДопустимЯНажимаюНаКнопку(string p0)
        {
            basicSteps.ClickLink(p0);
            Thread.Sleep(1000);
        }
        
        [When(@"я нажму на кнопку ""(.*)""")]
        public void ЕслиЯНажмуНаКнопку(string p0)
        {
            basicSteps.ClickLink(p0);
            Thread.Sleep(1000);
        }
        
        [Then(@"я вижу страницу ""(.*)""")]
        public void ТоЯВижуСтраницу(string p0)
        {
            basicSteps.IsElementFound(p0);
        }
        
        [Then(@"я заполняю форму для создания пользователя указывая имя ""(.*)""")]
        public void ТоЯЗаполняюФормуДляСозданияПользователяУказываяИмя(string p0)
        {
            basicSteps.SelectElement("Номер телефона", "Кыргызстан +996");
            basicSteps.ClickInput("phone");
            int number = run.Next(100000000, 999999999);
            basicSteps.FillText("phone", number.ToString());
            basicSteps.ClickInput("Name");
            basicSteps.FillText("Name", p0);
        }
        
        [Then(@"нажимаю кнопку ""(.*)""")]
        public void ТоНажимаюКнопку(string p0)
        {
            basicSteps.ClickButton("Создать");
        }
        
        [Then(@"я увижу ""(.*)""")]
        public void ТоЯУвижу(string p0)
        {
            basicSteps.IsElementFound(p0);
        }
    }
}
