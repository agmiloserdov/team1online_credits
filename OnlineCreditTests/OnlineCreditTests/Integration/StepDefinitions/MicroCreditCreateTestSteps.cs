﻿using TechTalk.SpecFlow;
using Xunit;


namespace OnlineCreditTests.Integration.StepDefinitions
{
    [Binding]
    public class СозданиеЗаявкиНаМикрокредитSteps
    {
        private BasicSteps basicSteps;
        public СозданиеЗаявкиНаМикрокредитSteps(BasicSteps basicSteps)
        {
            this.basicSteps = basicSteps;
        }

        [Given(@"я обычный пользователь")]
        public void ДопустимЯОбычныйПользователь()
        {
            basicSteps.GoToMainPage();
        }
            
        [Given(@"я вхожу в личный кабинет")]
        public void ДопустимЯВхожуВЛичныйКабинет()
        {

            basicSteps.SelectElement("Номер телефона", "Кыргызстан +996");
            basicSteps.ClickInput("phone");
            basicSteps.FillText("phone", "705014880");
            basicSteps.ClickButton("Отправить пароль");
            basicSteps.ClickButton("Вход");
        }
        
        [Then(@"я заполняю форму для получения кредита")]
        public void ТоЯЗаполняюФормуДляПолученияКредита()
        {
            basicSteps.SelectElement("Тип получения средств", "Пополнить баланс мобильного");
            basicSteps.FillTextNearLabel("Реквизиты", "9999999999", "text");
            basicSteps.SelectElement("Валюта", "Сом");
            basicSteps.FillTextNearLabel("Сумма займа", "1000", "text");
 
        }
        
        [Then(@"нажимаю кнопку оформить")]
        public void ТоНажимаюКнопкуОформить()
        {
            basicSteps.ClickButton("Оформить");
        }

        [AfterTestRun]
        public static void Close()
        {
            BasicSteps.driver.Quit();
            BasicSteps.driver.Close();
        }
    }
}
