﻿#language: ru

Функция: Тест измененения личных данных пользователя

Сценарий: Измененение имени пользователя
Допустим я нахожусь на странице  пользователей
И я   администратор
Допустим я нажимаю  на ссылку  "Roma"
Тогда я вижу  его личный кабинет
Тогда я нажимаю на инпут "Имя" и удаляю содержимое
И Ввожу новое имя "Roma"
Тогда я  нажимаю на кнопку "Сохранить изменения"
И вижу сообщение "Успех. Профиль был обновлен"