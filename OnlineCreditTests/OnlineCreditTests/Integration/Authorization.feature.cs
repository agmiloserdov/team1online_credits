// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.2.0.0
//      SpecFlow Generator Version:2.2.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace SpecFlow.GeneratedTests.Integration
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.2.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public partial class ЯХочуБытьАвторизованFeature : Xunit.IClassFixture<ЯХочуБытьАвторизованFeature.FixtureData>, System.IDisposable
    {
        
        private static TechTalk.SpecFlow.ITestRunner testRunner;
        
        private Xunit.Abstractions.ITestOutputHelper _testOutputHelper;
        
#line 1 "Authorization.feature"
#line hidden
        
        public ЯХочуБытьАвторизованFeature(ЯХочуБытьАвторизованFeature.FixtureData fixtureData, Xunit.Abstractions.ITestOutputHelper testOutputHelper)
        {
            this._testOutputHelper = testOutputHelper;
            this.TestInitialize();
        }
        
        public static void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("ru"), "Я хочу быть авторизован", null, ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        public static void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        public virtual void TestInitialize()
        {
        }
        
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<Xunit.Abstractions.ITestOutputHelper>(_testOutputHelper);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        void System.IDisposable.Dispose()
        {
            this.ScenarioTearDown();
        }
        
        [Xunit.FactAttribute(DisplayName="Открытие страницы регистрации")]
        [Xunit.TraitAttribute("FeatureTitle", "Я хочу быть авторизован")]
        [Xunit.TraitAttribute("Description", "Открытие страницы регистрации")]
        public virtual void ОткрытиеСтраницыРегистрации()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Открытие страницы регистрации", ((string[])(null)));
#line 5
this.ScenarioSetup(scenarioInfo);
#line 6
testRunner.Given("я нахожусь на главной странице", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Допустим ");
#line 7
testRunner.And("я анонимный пользователь", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "И ");
#line 8
testRunner.Given("я прохожу по ссылке \"Регистрация\"", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Допустим ");
#line 9
testRunner.Then("вижу страницу регистрации", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "То ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Xunit.FactAttribute(DisplayName="Регистрация нового пользователя")]
        [Xunit.TraitAttribute("FeatureTitle", "Я хочу быть авторизован")]
        [Xunit.TraitAttribute("Description", "Регистрация нового пользователя")]
        public virtual void РегистрацияНовогоПользователя()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Регистрация нового пользователя", ((string[])(null)));
#line 11
this.ScenarioSetup(scenarioInfo);
#line 12
testRunner.Given("я нахожусь на странице регистрации", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Допустим ");
#line 13
testRunner.Given("я заполняю регистрационные данные", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Допустим ");
#line 14
testRunner.Then("я вижу страницу входа", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "То ");
#line 15
testRunner.When("я ввожу пороль", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Когда ");
#line 16
testRunner.Then("я авторизуюсь", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Тогда ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.2.0.0")]
        [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
        public class FixtureData : System.IDisposable
        {
            
            public FixtureData()
            {
                ЯХочуБытьАвторизованFeature.FeatureSetup();
            }
            
            void System.IDisposable.Dispose()
            {
                ЯХочуБытьАвторизованFeature.FeatureTearDown();
            }
        }
    }
}
#pragma warning restore
#endregion
