﻿#language: ru

Функция: Тест блокировки пользователя

Сценарий: Блокировка пользователя
Допустим я нахожусь на странице пользователей
И я  администратор
Допустим я нажимаю на ссылку  "Roma"
Тогда я вижу его личный кабинет
Тогда я нажимаю на кнопку "Блок"
И вижу сообщение что пользователь заблокирован
Тогда я нажимаю на ссылку "Вернуться в профиль пользователя"
И я жму на кнопку "Разблокировать"
Тогда вижу сообщение о том что пользователь разблокирован