﻿ using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Server.Kestrel.Core;

namespace OnlineCreditsTeam1.Models
{
    public class Order
    {
        public int Id { get; set; }
        public string ClientId { get; set; }
        public string CorpClientId { get; set; }
        public decimal Sum { get; set; }
        public decimal UltimateSum { get; set; }
        public double PercentRateDown { get; set; }
        public string Status { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime FinishDate { get; set; }
        public string OrderInfoJson { get; set; }
        public DateTime DesiredDay { get; set; }
        public int Period { get; set; }
    }
}
