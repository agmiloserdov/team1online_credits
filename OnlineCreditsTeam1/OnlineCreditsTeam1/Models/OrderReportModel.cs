﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models
{
    public class OrderReportModel
    {
        public DateTime Date { get; set; }
        public string OrderStatus { get; set; }
    }
}
