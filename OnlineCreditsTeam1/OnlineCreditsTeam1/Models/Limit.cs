﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace OnlineCreditsTeam1.Models
{
    public class Limit
    {
        public int Id { get; set; }
        public string LimitType { get; set; }
        public string Period { get; set; }
        public DateTime CustomLimitBeginDate { get; set; }
        public DateTime CustomLimitFinishDate { get; set; }
        [Remote(action: "ValidateMaxOrdersOnPeriod", controller: "Validation", ErrorMessage = "Количество заявок должно быть от 0 до 9999")]
        public int? MaxOrdersOnPeriod { get; set; }
        public int LimitNumberCounter { get; set; }
        [Remote(action: "ValidateMaxSumOnPeriod", controller: "Validation", ErrorMessage = "Сумма должна быть от 0 до 99999999")]
        public double? MaxSumOnPeriod{ get; set; }
        public double LimitSumCounter { get; set; }
        [Remote(action: "ValidateMaxCreditSprint", controller: "Validation", ErrorMessage = "Количество месяцев должно быть от 0 до 24")]
        public int? MaxCreditSprint { get; set; }
        public bool isActive { get; set; }
        public string Currency { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }


    }
}
