﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models
{
    public class LogModel
    {
        public string LogMessage { get; set; }
        public DateTime ActionDate { get; set; }
        public string UserId { get; set; }
    }
}
