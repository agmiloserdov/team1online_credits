﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models
{
    public class LogViewModel
    {
        public string UserId { get; set; }
        public string Action { get; set; }
        public DateTime ActionDate { get; set; }

        public List<UserActionLogModel> Logs { get; set; }
    }
}
