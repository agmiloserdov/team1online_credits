﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models.DocumentsViewModels
{
    public class SertificateOfResidence
    {
        public DateTime DateOfCreate { get; set; }
        public string WhoIssued { get; set; }

        public string Address { get; set; }

        public string AmountOwed { get; set; }
    }
}
