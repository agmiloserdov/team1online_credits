﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OnlineCreditsTeam1.Services;

namespace OnlineCreditsTeam1.Models.DocumentsViewModels
{
    public class DocumentUploadViewModel
    {
        public IFormFile DocumentFile { get; set; }
        public string UserId { get; set; }
        public string DocumentTitle { get; set; }
        public int OrderId { get; set; }
        public string Json { get; set; }
        public DateTime DateOfReceive { get; set; }
        
        [Required(ErrorMessage = "Не заполнено поле выдающего органа.")]
        [RegularExpression(@"^[Mm][Kk]{2}(\s|)\d{2}-\d{2}$", ErrorMessage = "Должно быть в формате MKK xx-xx")]
        public string WhoTakeBlank { get; set; }
        public double DebtSum { get; set; }
        [StringLength(3, MinimumLength = 100, ErrorMessage = "Длина строки должна быть от 3 до 50 символов")]
        public string HomeAddress { get; set; }
        [RegularExpression(@"^[Aa][Nn](\s|)\d{8}$", ErrorMessage = "Должно быть в формате AN xxxxxxxx")]
        public string PassportNumber { get; set; }
        public double Salary { get; set; }
        public DateTime WorkBeginDate { get; set; }
        public PassportModel Passport { get; set; }
        public WorkBlankModel WorkBlank { get; set; }
        public HomeBlankModel HomeBlank { get; set; }
        public List<Document> Documents { get; set; }
    }
}
