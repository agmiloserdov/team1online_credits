﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models.DocumentsViewModels
{
    public class SertificateOfEmloyment
    {
        public DateTime DateOfCreate { get; set; }
        public string EmloyerInformation { get; set; }
        public DateTime DateOfStartWorking  { get; set; }
        public decimal SumOfSalary { get; set; }
    }
}
