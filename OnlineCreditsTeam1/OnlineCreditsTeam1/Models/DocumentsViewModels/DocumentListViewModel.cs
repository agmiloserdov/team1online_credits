﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace OnlineCreditsTeam1.Models.DocumentsViewModels
{
    public class DocumentListViewModel
    {
        public List<Document> Documents { get; set; }

        public IFormFile DocumentFile { get; set; }
        public string UserId { get; set; }
        public string DocumentTitle { get; set; }
        public int OrderId { get; set; }
        public string Json { get; set; }
    }
}
