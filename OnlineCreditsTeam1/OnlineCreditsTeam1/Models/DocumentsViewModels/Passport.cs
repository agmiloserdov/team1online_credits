﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models.DocumentsViewModels
{
    public class Passport
    {
        public string NumberOfPassport { get; set; }
        public string WhoIssued { get; set; }
        public DateTime DateOfCreate { get; set; }
    }
}
