﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models
{
    public class GetReportModel
    {
        public string UserId { get; set; }
        public DateTime BeginDate { get; set; } = DateTime.Now;
        public DateTime FinishDate { get; set; } = DateTime.MaxValue;
        public bool ConfirmedOrders { get; set; }
        public bool DeclinedOrders { get; set; }
        public bool SellDynamics { get; set; }
    }
}
