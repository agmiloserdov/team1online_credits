﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace OnlineCreditsTeam1.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        
        public string PhotoPath { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string Requisits { get; set; }
        public DateTime CreateTime { get; set; }
        public string Role { get; set; }
        public IEnumerable<Limit> Limits{ get; set; }
        public string LogFilePath { get; set; }
        // public List<Documents> Documents { get; set; }
        public string CreditAdminId { get; set; }
        public ApplicationUser CreditAdmin { get; set; }
        public string SiteAddres { get; set; }

    }
}
