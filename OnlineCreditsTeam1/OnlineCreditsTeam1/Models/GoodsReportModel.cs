﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models
{
    public class GoodsReportModel
    {
        public double Price { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }

    }
}
