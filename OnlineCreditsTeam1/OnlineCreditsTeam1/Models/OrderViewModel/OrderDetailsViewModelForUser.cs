﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models.OrderViewModel
{
    public class OrderDetailsViewModelForUser
    {
        public string CorpClientName { get; set; }
        public decimal Sum { get; set; }
        public string Status { get; set; }
        public DateTime FinishDate { get; set; }
        public DateTime BeginDate { get; set; }
        public int MonthCount { get; set; }
    }
}
