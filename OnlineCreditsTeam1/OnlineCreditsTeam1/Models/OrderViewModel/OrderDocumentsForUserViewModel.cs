﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models.OrderViewModel
{
    public class OrderDocumentsForUserViewModel
    {
        public List<Document> OrderDocuments { get; set; }

    }
}
