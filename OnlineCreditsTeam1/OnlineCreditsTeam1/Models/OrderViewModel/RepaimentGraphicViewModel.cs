﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models.OrderViewModel
{
    public class RepaimentGraphicViewModel
    {
        public decimal SumOfCredit { get; set; }
        public DateTime StartRepaimentDate { get; set; }
        public DateTime FinishRepaimentDate { get; set; }
        public List<DateTime> PeriodsOfRepaiment { get; set; }
        public List<decimal> SumsOfCreditForPeriod { get; set; }
    }
}
