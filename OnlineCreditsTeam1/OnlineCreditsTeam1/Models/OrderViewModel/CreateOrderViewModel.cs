﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models.OrderViewModel
{
    public class CreateOrderViewModel
    {
        public string MarketId { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string FatherName { get; set; }

        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string OrderInfoJson { get; set; }
        public string DesiredDay {get; set;}

        public int Sum { get; set; }

        public int Period { get; set; }
    }
}
