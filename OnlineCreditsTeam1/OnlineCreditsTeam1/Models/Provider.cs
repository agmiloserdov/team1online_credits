﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models
{
    public class Provider
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PaymentId { get; set; }
        public Payment Payment { get; set; }
    }
}
