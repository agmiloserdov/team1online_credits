﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OnlineCreditsTeam1.Services;

namespace OnlineCreditsTeam1.Models.ManageViewModels
{
    public class UserProfileViewModel
    {
        public string UserName { get; set; }
        public string Id { get; set; }
        public string Status { get; set; }
        public string PhotoPath { get; set; }
        public DateTime CreateDate { get; set; }
        public IFormFile PhotoFile { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string AccountType { get; set; }
        public RequisitViewModel Requisits { get; set; }
        public string Role { get; set; }
        public InterestRate InterestRate { get; set; }
        public PreferentialInterestRate PreferentialInterestRate { get; set; }
        public MicroCredit Credit { get; set; }
    }
}
