﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
namespace OnlineCreditsTeam1.Models
{
    public class Document
    {
        public string Id { get; set; }
        public string UserId { get; set; }

        public int OrderId { get; set; }
        public Order Order { get; set; }
        public string DeclineMessage { get; set; }
        public string Title { get; set; }
        public string Path { get; set; }
        public bool IsApproved { get; set; }
        public string Json { get; set; }
    }
}
