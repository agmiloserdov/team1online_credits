﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models
{
    public class Payment
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Term { get; set; }
        public IEnumerable<Provider> Providers { get; set; }
    }
}
