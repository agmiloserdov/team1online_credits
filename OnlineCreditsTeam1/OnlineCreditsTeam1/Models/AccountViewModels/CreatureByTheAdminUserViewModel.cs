﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models.AccountViewModels
{
    public class CreatureByTheAdminUserViewModel
    {
        public string UserId { get; set; }
        public List<IdentityRole> AllRoles { get; set; }
        [Required (ErrorMessage = "Укажите номер телефона")]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }
        [Required (ErrorMessage = "Укажите имя")]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "Длина имени должна быть от 2 до 30 символов")]
        public string Name { get; set; }
        [StringLength(12, MinimumLength = 10, ErrorMessage = "Длина ИНН должна быть от 10 до 12 чисел")]
        public string Inn { get; set; }
        [StringLength(10, MinimumLength = 8, ErrorMessage = "Длина БИК должна быть от 8 до 10 символов")]
        public string Bik { get; set; }
        [StringLength(100, MinimumLength = 5, ErrorMessage = "Длина юридического адреса должна быть от 5 до 100 символов")]
        public string LegalAdress { get; set; }
        [StringLength(20, MinimumLength = 18, ErrorMessage = "Длина банковского счета должна быть от 18 до 20 символов")]
        public string BankAccountNumber { get; set; }
        public string Role { get; set; }
        public string CreditAdminId { get; set; }
        public List<ApplicationUser> AllBankOperators { get; set; }


    }
}
