﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models.AccountViewModels
{
    public class LoginViewModel
    {

        [Phone]
        [StringLength(20, ErrorMessage = "Номер  должен быть от {2} до {1} цифр", MinimumLength = 10)]
        public string PhoneNumber { get; set; }

        
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public string Message { get; set; }

    }
}
