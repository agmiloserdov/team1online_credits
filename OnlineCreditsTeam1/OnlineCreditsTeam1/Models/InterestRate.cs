﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models
{
    public class InterestRate
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Обязательное поле! Дата начала действия ставки не должна быть меньше текущей даты")]
        public DateTime BeginDate { get; set; }
        [Required(ErrorMessage = "Обязательное поле! Дата окончания действия ставки не должна быть раньше даты начала действия + 1 день")]
        public DateTime FinishDate { get; set; }
        [Range(1, 24, ErrorMessage = "Недопустимое количество месяцев")]
        public int? CreditTermFrom { get; set; }
        [Range(1, 24, ErrorMessage = "Недопустимое количество месяцев")]
        public int? CreditTermTo { get; set; }
        public int CurrencyCode { get; set; }
        [Range(1, 9999999, ErrorMessage = "Недопустимая сумма кредита")]
        public double? TotalSumFrom { get; set; }
        [Range(1, 9999999, ErrorMessage = "Недопустимая сумма кредита")]
        public double? TotalSumTo { get; set; }
        public string CorpClientId { get; set; }
        public ApplicationUser CorpClient { get; set; }
        [Required(ErrorMessage = "Обязательное поле! Размер процентной ставки должен быть установлен (от 1 до 100 процентов)")]
        [Range(1, 100, ErrorMessage = "Недопустимый процент кредитной ставки")]
        public double PercentRate { get; set; }

    }
}
