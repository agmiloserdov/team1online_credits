﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models.SettingsViewModels
{
    public class SettingViewModel
    {
        [Range(128, 8096)]
        public int FileSize { get; set; }

        [Range(1, 30)]
        public int AutoCancelOrder { get; set; }

        public int SumAutoAcceptOrder { get; set; }
    }
}
