﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models
{
    public class MicroCredit
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public double Sum { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime FinishDate { get; set; }
        public string Requisite { get; set; }
        public string Description { get; set; }
        public string Status { get; set; } = "new";
        public DateTime OrderCreationDate { get; set; } = DateTime.Now;
        public int OrderTerm { get; set; }
        public string Currency { get; set; }
    }
}
