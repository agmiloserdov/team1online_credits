﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models
{
    public class OrderInfo
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public string Currency { get; set; }
    }
}
