﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models
{
    public class UserActionLogModel
    {
        public string Id { get; set; }

        [ForeignKey("ApplicationUser")]
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public string Action { get; set; }
        public DateTime ActionDate { get; set; }
    }
}
