﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models
{
    public class ReportModel
    {
        public DateTime BeginDate { get; set; }
        public decimal Sum { get; set; }
        public string UserId { get; set; }
        public string OrderStatus { get; set; }

    }
}
