﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models
{
    public class OrderConfirmation
    {
        public string Id { get; set; }
        public int  OrderId { get; set; }
        public Order Order { get; set; }
        public bool IsUserConfirm { get; set; }
        public bool IsAdminConfirm { get; set; }
        public string UserDeclineMessage { get; set; }
        public string AdminDeclineMessage { get; set; }
    }
}
