﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Models
{
    public interface IRepository
    {
        void SaveChanges();
        void Update(ApplicationUser user);
        ApplicationUser GetUserById(string userId);
    }
}
