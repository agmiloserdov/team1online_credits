﻿using System;

namespace OnlineCreditsTeam1.Services
{
    public class PasswordGenerator
    {
        /// <summary>
        /// Метод производит генерацию пароля
        /// </summary>
        /// <param name="length">Параметр length задает количество символов в пароле</param>
        /// <returns>Возвращает пароль длиной равной length</returns>
        public string GeneratePassword(int length)
        {
            string password = "";
            Random rnd = new Random();
            for (int a = 0; a < length; a++)
            {
                int pwdNum = rnd.Next(0, 9);
                password += pwdNum.ToString();
            }

            return password;
        }
    }
}