﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using OnlineCreditsTeam1.Data;
using OnlineCreditsTeam1.Models;

namespace OnlineCreditsTeam1.Services
{
    public class UserLogWriter
    {
        readonly IRepository _repository;

        public UserLogWriter(IRepository repository)
        {
            _repository = repository;
        }
        private string FilePathBuilder(string userId)
        {
            string logFilePath = $"wwwroot/logs/{userId}.json";
            return logFilePath;
        }
        public void WriteLog(LogModel model)
        {
            List<LogModel> logs = new List<LogModel>();
            if (IsFileExist(FilePathBuilder(model.UserId)))
            {
                logs = GetAllLogs(model.UserId);
                logs.Add(model);
                File.WriteAllText(FilePathBuilder(model.UserId), JsonConvert.SerializeObject(logs));
            }
            else
            {
                logs.Add(model);
                File.WriteAllText(FilePathBuilder(model.UserId), JsonConvert.SerializeObject(logs));
                ApplicationUser user = _repository.GetUserById(model.UserId);
                user.LogFilePath = FilePathBuilder(user.Id);
                _repository.Update(user);
                _repository.SaveChanges();
            }
        }

        

        private bool IsFileExist(string path)
        {
            return File.Exists(path);
        }


        public List<LogModel> GetAllLogs(string userId)
        {
            ApplicationUser user = _repository.GetUserById(userId);
            List<LogModel> logs = new List<LogModel>();
            try
            {
                string userLogs = File.ReadAllText(user.LogFilePath);
                logs = JsonConvert.DeserializeObject<List<LogModel>>(userLogs);
            }
            catch (Exception e)
            {
                logs.Add(new LogModel{ActionDate = DateTime.Parse("0001-01-01T00:00:00.0000000"), LogMessage = "Demo message", UserId = "Demo Id"});
            }
            return logs;
        }
    }
   
}
