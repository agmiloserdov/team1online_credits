﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using OnlineCreditsTeam1.Controllers;
using OnlineCreditsTeam1.Data;
using OnlineCreditsTeam1.Models;
using OnlineCreditsTeam1.Models.DocumentsViewModels;
using OnlineCreditsTeam1.Models.ManageViewModels;

namespace OnlineCreditsTeam1.Services
{
    public class DocumentLoader
    {
        private readonly ApplicationDbContext _context;
        private readonly FileUploadService _fileUploadService;
        private readonly IHostingEnvironment _environment;
        
        public DocumentLoader(
            ApplicationDbContext context,
            FileUploadService fileUploadService,
            IHostingEnvironment environment)
        {
            _context = context;
            _fileUploadService = fileUploadService;
            _environment = environment;
        }

        public Document UploadScan(DocumentUploadViewModel model)
        {
            SettingsService service = new SettingsService();
            long size = (long)service.GetSettnigs().FileSize;
            if (size < model.DocumentFile.Length / 1024)
            {

                return null;
            }
            string pathDocuments = Path.Combine(_environment.WebRootPath,$"DynamicsData\\{model.UserId}\\{model.OrderId}\\Documents\\");
            string documentFile = $"/DynamicsData/{model.UserId}/{model.OrderId}/Documents/{model.DocumentFile.FileName}";
            string pattern = @"^.*\.(jpg|JPG|jpeg|png)$";
            if (Regex.IsMatch(documentFile, pattern, RegexOptions.IgnoreCase))
            {
                _fileUploadService.Upload(pathDocuments, model.DocumentFile.FileName, model.DocumentFile);
            }
            else
            {
                return null;
            }

            
           
            Document doc = new Document()
            {
                IsApproved = false,
                Json = model.Json,
                OrderId = model.OrderId,
                UserId = model.UserId,
                Path = documentFile,
                Title = model.DocumentTitle
            };
            
            _context.Add(doc);
            _context.SaveChangesAsync();

            return doc;
        }

        
    }
}
