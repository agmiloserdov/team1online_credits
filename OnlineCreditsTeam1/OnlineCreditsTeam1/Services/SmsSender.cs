﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using OnlineCreditsTeam1.Data;
using OnlineCreditsTeam1.Models;
using SQLitePCL;

namespace OnlineCreditsTeam1.Services
{
    public class SmsSender
    {
        private readonly ApplicationDbContext _context;

        public SmsSender(
            ApplicationDbContext context)
        {
            _context = context;
        }

        public string GetUserNumberById(string userId)
        {
            ApplicationUser user = new ApplicationUser();
            string phoneNumber = "";
            try
            {
                user = _context.Users.First(u => u.Id == userId);
                phoneNumber = user.PhoneNumber;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return phoneNumber;
        }

        public void SendSms(string message, string phoneNumber)
        {
            Console.WriteLine($"Message for : {phoneNumber} \nMessage : {message}");
        }
        
    }
}
