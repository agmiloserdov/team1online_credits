﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Localization;
using OnlineCreditsTeam1.Controllers;
using OnlineCreditsTeam1.Data;
using OnlineCreditsTeam1.Models;

namespace OnlineCreditsTeam1.Services
{
    [Authorize]
    public class OrderChecker
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public OrderChecker(
            ApplicationDbContext context,
            SignInManager<ApplicationUser> signInManager,
            UserManager<ApplicationUser> userManager)
        {
            _signInManager = signInManager;
                _context = context;
                _userManager = userManager;
            }
        public bool CheckOrderConfirmationEntity(int orderId)
        {
            var result = false;
            try
            {
                var orderConfirmation = _context.OrderConfirmations.First(o => o.OrderId == orderId);
                if (orderConfirmation != null)
                {
                    result = true;
                }
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }

        public OrderConfirmation CreateOrderConfirmationEntity(int orderId)
        {
            OrderConfirmation orderConfirmation = new OrderConfirmation
            {
                IsAdminConfirm = false,
                IsUserConfirm = false,
                AdminDeclineMessage = "",
                UserDeclineMessage = "",
                OrderId = orderId
            };
            _context.OrderConfirmations.Add(orderConfirmation);
            _context.SaveChanges();
            return orderConfirmation;
        }

        
    }
}
