﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

using CsvHelper;

using Newtonsoft.Json;

using OnlineCreditsTeam1.Models;

namespace OnlineCreditsTeam1.Services
{
    public class ReportGenerator
    {
        

        public string GetReportLink(List<Order> orders)
        {
            string reportPath = "~/Reports/";
            string jsonReport = "";
            try
            {
                if (orders.Count == 0)
                {
                    return "";
                }
                List<ReportModel> reportList = CreateNewList(orders);
                Console.WriteLine(string.Join(" ", reportList));
                string csv = ListToCsv(reportList, ",");

                reportPath += $"{orders[0].CorpClientId}_report.csv";
                string filePath = $"wwwroot/Reports/{orders[0].CorpClientId}_report.csv";
                if (File.Exists(filePath))
                {
                    RemoveFile(filePath);
                }
                File.WriteAllText(filePath, csv);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return reportPath;
        }

        private void RemoveFile(string path)
        {
            File.Delete(path);
        }

        private List<ReportModel> CreateNewList(List<Order> orders)
        {
            List<ReportModel> reportList = new List<ReportModel>();
            foreach (var order in orders)
            {
                reportList.Add(new ReportModel { BeginDate = order.BeginDate, UserId = order.ClientId, Sum = order.Sum, OrderStatus = order.Status });
            }

            return reportList;
        }

        private  DataTable JsonStringToTable(string jsonContent)
        {
            DataTable dt = JsonConvert.DeserializeObject<DataTable>(jsonContent);
            return dt;
        }

        public string ListToCsv(List<ReportModel> list, string delimiter)
        {
            var jsonString = JsonConvert.SerializeObject(list);
            StringWriter csvString = new StringWriter();
            using (var csv = new CsvWriter(csvString))
            {
                csv.Configuration.Delimiter = delimiter;

                using (var dt = JsonStringToTable(jsonString))
                {
                    foreach (DataColumn column in dt.Columns)
                    {
                        csv.WriteField(column.ColumnName);
                    }
                    csv.NextRecord();

                    foreach (DataRow row in dt.Rows)
                    {
                        for (var i = 0; i < dt.Columns.Count; i++)
                        {
                            csv.WriteField(row[i]);
                        }
                        csv.NextRecord();
                    }
                }
            }
            return csvString.ToString();
        }

        public string ListToCsv(List<Order> list, string delimiter)
        {
            var jsonString = JsonConvert.SerializeObject(list);
            StringWriter csvString = new StringWriter();
            using (var csv = new CsvWriter(csvString))
            {
                csv.Configuration.Delimiter = delimiter;

                using (var dt = JsonStringToTable(jsonString))
                {
                    foreach (DataColumn column in dt.Columns)
                    {
                        csv.WriteField(column.ColumnName);
                    }
                    csv.NextRecord();

                    foreach (DataRow row in dt.Rows)
                    {
                        for (var i = 0; i < dt.Columns.Count; i++)
                        {
                            csv.WriteField(row[i]);
                        }
                        csv.NextRecord();
                    }
                }
            }
            return csvString.ToString();
        }
    }
}
