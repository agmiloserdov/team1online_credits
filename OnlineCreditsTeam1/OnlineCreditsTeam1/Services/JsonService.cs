﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OnlineCreditsTeam1.Models;
using OnlineCreditsTeam1.Models.AccountViewModels;
using OnlineCreditsTeam1.Models.DocumentsViewModels;

namespace OnlineCreditsTeam1.Services
{
    public class JsonService
    {
        private readonly ILogger _logger;

        public JsonService(ILogger<JsonService> logger)
        {
            _logger = logger;
        }

        public JsonService()
        {
            
        }

        public string CreateJson(CreatureByTheAdminUserViewModel model)
        {
            string output = "";
            if (model.Role == "MarketOperator")
            {
                List<MarketRequisitViewModel> requisits = new List<MarketRequisitViewModel>();
                requisits.Add(new MarketRequisitViewModel()
                {

                    Bik = model.Bik,
                    BankAccountNumber = model.BankAccountNumber,
                    LegalAdress = model.LegalAdress
                });
                
                     output = JsonConvert.SerializeObject(requisits);
            }
            if (model.Role == "BankOperator" ||model.Role == "BankOperator")
            {
                List<AdminBankRequisitViewModel> requisits = new List<AdminBankRequisitViewModel>();
                requisits.Add(new AdminBankRequisitViewModel()
                {

                    Inn = model.Inn,
                    
                });
                
             output = JsonConvert.SerializeObject(requisits);

                
            }
            return output;
        }
      

        public RequisitViewModel GetJson(string content)
        {
            RequisitViewModel modelOut = new RequisitViewModel();

            try
            {
                List<RequisitViewModel> model = JsonConvert.DeserializeObject<List<RequisitViewModel>>(content);
                modelOut = model[0];
                return modelOut;
            }
            catch (Exception e)
            {
                _logger?.LogWarning("Requisits not found");
                return modelOut;
            }
            
        }

        public string CreateJson(DocumentUploadViewModel model)
        {
            string output = "";
            if (model.DocumentTitle == "passportFront" || model.DocumentTitle == "passportBack")
            {
                PassportModel requisits = new PassportModel()
                {
                    DateOfReceive = model.DateOfReceive,
                    PassportNumber = model.PassportNumber,
                    WhoTakeBlank = model.WhoTakeBlank
                };
                output = JsonConvert.SerializeObject(requisits);

            }
            else if (model.DocumentTitle == "workBlank")
            {
                WorkBlankModel requisits = new WorkBlankModel()
                {
                    DateOfReceive = model.DateOfReceive,
                    WorkBeginDate = model.WorkBeginDate,
                    WhoTakeBlank = model.WhoTakeBlank,
                    Salary = model.Salary
                };
                output = JsonConvert.SerializeObject(requisits);
            }
            else
            {
                HomeBlankModel requisits = new HomeBlankModel()
                {
                    DateOfReceive = model.DateOfReceive,
                    HomeAddress = model.HomeAddress,
                    WhoTakeBlank = model.WhoTakeBlank,
                    DebtSum = model.DebtSum
                };
                output = JsonConvert.SerializeObject(requisits);
            }
            return output;
        }
    }

    public class PassportModel
    {
        public DateTime DateOfReceive { get; set; }
        public string PassportNumber { get; set; }
        public string WhoTakeBlank { get; set; }
    }
    public class WorkBlankModel
    {
        public DateTime DateOfReceive { get; set; }
        public DateTime WorkBeginDate { get; set; }
        public string WhoTakeBlank { get; set; }
        public double Salary { get; set; }
    }
    public class HomeBlankModel
    {
        public DateTime DateOfReceive { get; set; }
        public double DebtSum { get; set; }
        public string HomeAddress { get; set; }
        public string WhoTakeBlank { get; set; }
    }

    public class MarketRequisitViewModel
    {
        public string Bik { get; set; }
        public string BankAccountNumber { get; set; }
        public string LegalAdress { get; set; }
    }
    public class AdminBankRequisitViewModel
    {
        public string Inn { get; set; }
    }
    public class RequisitViewModel
    {
        public string Bik { get; set; }
        public string BankAccountNumber { get; set; }
        public string LegalAdress { get; set; }
        public string Inn { get; set; }
    }
}




