﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OnlineCreditsTeam1.Models.SettingsViewModels;

namespace OnlineCreditsTeam1.Services
{
    public class DataLoader
    {
        private string directory;
        private string fileName;

        public DataLoader(string path, string fileName)
        {
            this.directory = path;
            this.fileName = fileName;
        }
        

        public void SerializeList(SettingViewModel settings)
        {
            string json = JsonConvert.SerializeObject(settings);
            SaveFile(json);
        }

        public SettingViewModel LoadDeserializedList()
        {
            try
            {
                string content = File.ReadAllText($"{directory}/{fileName}");
                if (string.IsNullOrEmpty(content))
                {
                    throw new ApplicationException($"файл {fileName} пустой");
                }
                
                return JsonConvert.DeserializeObject<SettingViewModel>(content);
            }
            catch (DirectoryNotFoundException)
            {
                Console.WriteLine($"Отсутствует директория {directory}");
                return new SettingViewModel();
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine($"Отсутствует файл {fileName}");
                return new SettingViewModel();
            }
            catch (ApplicationException ex)
            {
                Console.WriteLine(ex.Message);
                return new SettingViewModel();
            }
        }

        private void SaveFile(string content)
        {
            try
            {
                File.WriteAllText($"{directory}/{fileName}", content);
            }
            catch (DirectoryNotFoundException)
            {
                Directory.CreateDirectory(directory);
                SaveFile(content);
            }
        }
    }
}
