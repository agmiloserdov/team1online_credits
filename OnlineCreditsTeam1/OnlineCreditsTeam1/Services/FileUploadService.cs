﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineCreditsTeam1.Services
{
    public class FileUploadService
    {
        /// <summary>
        /// Метод копирует файл в указанную директорию
        /// </summary>
        /// <param name="path">Путь до того места куда нужно скопировать файл</param>
        /// <param name="fileName">Название файла файла</param>
        /// <param name="file">Виджет который принемает файл с формы</param>
        public async void Upload(string path, string fileName, IFormFile file)
        {
            SettingsService service = new SettingsService();

            long size = (long) service.GetSettnigs().FileSize;
            if (file.Length/1024 <= size)
            {
                Directory.CreateDirectory(path);
                using (var stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }
            else { }
        }
    }
}
