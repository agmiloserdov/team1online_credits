﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using OnlineCreditsTeam1.Data;
using OnlineCreditsTeam1.Models;

namespace OnlineCreditsTeam1.Services
{
    
    public class UserActionLogger
    {
        readonly ILogRepository _repository;

        public UserActionLogger(ILogRepository repository)
        {
            _repository = repository;
        }


        public void SaveActionInLog(LogViewModel model)
        {
            UserActionLogModel logModel = new UserActionLogModel
            {
                Action = model.Action,
                ActionDate = model.ActionDate,
                UserId = model.UserId
            };
            _repository.Add(logModel);
            _repository.SaveChanges();
        }

        public async Task<LogViewModel> LoadUserLog(string userId)
        {
            var userLogs = _repository.GetLogsById(userId);
            LogViewModel model = new LogViewModel
            {
                Logs = userLogs
            };
            return model;
        }
        public async Task<LogViewModel> LoadUserLog()
        {
            var userLogs = _repository.GetAllLogs();
            LogViewModel model = new LogViewModel
            {
                Logs = userLogs
            };
            return model;
        }

    }
}
