﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using OnlineCreditsTeam1.Controllers;
using OnlineCreditsTeam1.Models;

namespace OnlineCreditsTeam1.Services
{
    public class DbService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger _logger;
        private PasswordGenerator _passwordGenerator;
        private readonly RoleManager<IdentityRole> _roleManager;

        public DbService(PasswordGenerator passwordGenerator,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<AccountController> logger,
            RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _passwordGenerator = passwordGenerator;
            _signInManager = signInManager;
            _logger = logger;
            _roleManager = roleManager;
        }
    }

}
