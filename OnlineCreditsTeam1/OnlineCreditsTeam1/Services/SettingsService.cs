﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OnlineCreditsTeam1.Extensions;
using OnlineCreditsTeam1.Models.SettingsViewModels;

namespace OnlineCreditsTeam1.Services
{
    public class SettingsService
    {
        private DataLoader dataLoader = new DataLoader("~/..", "settings.json");

        /// <summary>
        /// Возвращает список настроек в формате
        /// </summary>
        /// <returns>Словарь с настройками</returns>
        public SettingViewModel GetSettnigs()
        {
            
            try
            {
                SettingViewModel settings = dataLoader.LoadDeserializedList();
                if (settings == null)
                {
                    throw new NullReferenceException("На данный момент нет настроек которые мы можем отобразить для вас");
                }
                return settings;
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine(e);
                return new SettingViewModel();
            }
        }


        public void SaveSettings(SettingViewModel settings)
        {
            DataLoader dataLoader = new DataLoader("~/..", "settings.json");

            try
            {
                dataLoader.SerializeList(settings);
        
            }
            catch (NullReferenceException e)
            {
                dataLoader.LoadDeserializedList();
                
            }
        }
    }
}
