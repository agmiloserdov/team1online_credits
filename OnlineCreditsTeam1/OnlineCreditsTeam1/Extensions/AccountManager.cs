﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using OnlineCreditsTeam1.Controllers;
using OnlineCreditsTeam1.Models;
using OnlineCreditsTeam1.Models.AccountViewModels;
using OnlineCreditsTeam1.Services;

namespace OnlineCreditsTeam1.Extensions
{
    public class AccountManager
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger _logger;
        private PasswordGenerator _passwordGenerator;

        public AccountManager(PasswordGenerator passwordGenerator,
            UserManager<ApplicationUser> userManager,
            ILogger<AccountController> logger)
        {
            _userManager = userManager;
            _passwordGenerator = passwordGenerator;
            _logger = logger;
        }

        /// <summary>
        /// Создает объект юзер
        /// </summary>
        /// <param name="phoneNumber">Номер телефона</param>
        /// <param name="role">Роль</param>
        /// <param name="name">Имя</param>
        /// <param name="email">Адрес эл.почты</param>
        /// <param name="requisits">Реквизиты</param>
        /// <returns>Объект Application User, готовый к сохранению в DB</returns>
        public ApplicationUser CreateUser(string phoneNumber, string role, string name, string email, string requisits, string creditAdminId)
        {
            string photoPath = "/StaticData/Anonymous/no_product.png";

            var user = new ApplicationUser
            {
                Role = role,
                UserName = phoneNumber,
                PhoneNumber = phoneNumber,
                Email = email,
                PhotoPath = photoPath,
                Requisits = requisits,
                Name = name,
                CreateTime = DateTime.Now,
                CreditAdminId = creditAdminId,
                Status = "Активен"
            };

            return user;
        }

        /// <summary>
        /// Асинхронный метод создания пользователя
        /// </summary>
        /// <param name="user">Application User</param>
        /// <returns>bool</returns>
        public async Task<bool> SaveUserAsync(ApplicationUser user)
        {
            try
            {
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    _logger.LogInformation($"Создан пользователь {user.Name};");
                }

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError($"Не удалось создать пользователя {user.Name}" );
                _logger.LogError(e.StackTrace);
                return false;
            }
        }

        /// <summary>
        /// Метод заглушка для отправки пароля пользователю, по смс
        /// </summary>
        /// <param name="model">Приемает модель с заполненным номером телефона</param>
        /// <returns>Возвращает номер телефона и пароль к нему</returns>
        public async Task<LoginViewModel> SendPassword(LoginViewModel model)
        {
            ApplicationUser user = await _userManager.FindByNameAsync(model.PhoneNumber);
            var password = _passwordGenerator.GeneratePassword(6);

            // ДАНГЕР!!!!!!!!!!!!!!!!!!!!! 
            // Опасность !!!! 
            // Перед выводом в продакшн удалить нижестоящую строку 
            _logger.LogInformation($"User has new password {password}");
            await _userManager.RemovePasswordAsync(user);
            await _userManager.AddPasswordAsync(user, password);

            return new LoginViewModel { Password = password, PhoneNumber = model.PhoneNumber };

        }


        /// <summary>
        /// Асинхронный метод назначения роли пользователю
        /// </summary>
        /// <param name="user"></param>
        /// <param name="roles"></param>
        /// <returns>если true - роль присвоилась, иначе false</returns>
        public async Task<bool> AddToRoleAsync(ApplicationUser user, string role)
        {
            try
            {
                if (role == null)
                {
                    throw new NullReferenceException(message: "Не указана роль для присвоения!");
                }

                var result = await _userManager.AddToRolesAsync(user, new List<string>().Append(role));
                
                if (result.Succeeded)
                {
                    _logger.LogInformation($"Установлена роль {role} для {user.Name}:{user.Id}");
                }

                return true;
            }
            catch (Exception e)
            {
                _logger.LogError($"Не удалось установить роль для {user.Name}");
                _logger.LogError(e.ToString());
                return false;
            }
        }
    }
}
