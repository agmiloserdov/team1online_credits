﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace OnlineCreditsTeam1.Migrations
{
    public partial class AddCurrencyFieldToCredit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PreferentialRates_AspNetUsers_CorpClientId",
                table: "PreferentialRates");

            migrationBuilder.AlterColumn<string>(
                name: "CorpClientId",
                table: "PreferentialRates",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<string>(
                name: "Currency",
                table: "MicroCredits",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PreferentialRates_AspNetUsers_CorpClientId",
                table: "PreferentialRates",
                column: "CorpClientId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PreferentialRates_AspNetUsers_CorpClientId",
                table: "PreferentialRates");

            migrationBuilder.DropColumn(
                name: "Currency",
                table: "MicroCredits");

            migrationBuilder.AlterColumn<string>(
                name: "CorpClientId",
                table: "PreferentialRates",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PreferentialRates_AspNetUsers_CorpClientId",
                table: "PreferentialRates",
                column: "CorpClientId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
