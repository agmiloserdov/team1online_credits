﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace OnlineCreditsTeam1.Migrations
{
    public partial class AddPreferentialRatesEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PreferentialRates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BeginDate = table.Column<DateTime>(nullable: false),
                    CorpClientId = table.Column<string>(nullable: false),
                    CreditTermFrom = table.Column<int>(nullable: false),
                    CreditTermTo = table.Column<int>(nullable: false),
                    CurrencyCode = table.Column<int>(nullable: false),
                    FinishDate = table.Column<DateTime>(nullable: true),
                    PercentRateDown = table.Column<double>(nullable: false),
                    TotalSumFrom = table.Column<double>(nullable: false),
                    TotalSumTo = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PreferentialRates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PreferentialRates_AspNetUsers_CorpClientId",
                        column: x => x.CorpClientId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PreferentialRates_CorpClientId",
                table: "PreferentialRates",
                column: "CorpClientId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PreferentialRates");
        }
    }
}
