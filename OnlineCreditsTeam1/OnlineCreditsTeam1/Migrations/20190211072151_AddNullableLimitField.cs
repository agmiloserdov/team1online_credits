﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace OnlineCreditsTeam1.Migrations
{
    public partial class AddNullableLimitField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PreferentialRates_AspNetUsers_CorpClientId",
                table: "PreferentialRates");

            migrationBuilder.AlterColumn<string>(
                name: "CorpClientId",
                table: "PreferentialRates",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<double>(
                name: "MaxSumOnPeriod",
                table: "Limit",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<int>(
                name: "MaxOrdersOnPeriod",
                table: "Limit",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "MaxCreditSprint",
                table: "Limit",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_PreferentialRates_AspNetUsers_CorpClientId",
                table: "PreferentialRates",
                column: "CorpClientId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PreferentialRates_AspNetUsers_CorpClientId",
                table: "PreferentialRates");

            migrationBuilder.AlterColumn<string>(
                name: "CorpClientId",
                table: "PreferentialRates",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "MaxSumOnPeriod",
                table: "Limit",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MaxOrdersOnPeriod",
                table: "Limit",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MaxCreditSprint",
                table: "Limit",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PreferentialRates_AspNetUsers_CorpClientId",
                table: "PreferentialRates",
                column: "CorpClientId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
