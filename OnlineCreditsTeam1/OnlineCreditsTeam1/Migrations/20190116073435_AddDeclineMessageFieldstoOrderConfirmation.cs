﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace OnlineCreditsTeam1.Migrations
{
    public partial class AddDeclineMessageFieldstoOrderConfirmation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AdminDeclineMessage",
                table: "OrderConfirmations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserDeclineMessage",
                table: "OrderConfirmations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdminDeclineMessage",
                table: "OrderConfirmations");

            migrationBuilder.DropColumn(
                name: "UserDeclineMessage",
                table: "OrderConfirmations");
        }
    }
}
