﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace OnlineCreditsTeam1.Migrations
{
    public partial class AddInterestRateEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InterestRates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BeginDate = table.Column<DateTime>(nullable: false),
                    CorpClientId = table.Column<string>(nullable: true),
                    CreditTermFrom = table.Column<int>(nullable: false),
                    CreditTermTo = table.Column<int>(nullable: false),
                    CurrencyCode = table.Column<int>(nullable: false),
                    FinishDate = table.Column<DateTime>(nullable: true),
                    PercentRate = table.Column<double>(nullable: false),
                    TotalSumFrom = table.Column<double>(nullable: false),
                    TotalSumTo = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InterestRates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_InterestRates_AspNetUsers_CorpClientId",
                        column: x => x.CorpClientId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InterestRates_CorpClientId",
                table: "InterestRates",
                column: "CorpClientId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InterestRates");
        }
    }
}
