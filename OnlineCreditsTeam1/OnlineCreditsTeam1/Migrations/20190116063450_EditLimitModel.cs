﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace OnlineCreditsTeam1.Migrations
{
    public partial class EditLimitModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isPeriodical",
                table: "Limit");

            migrationBuilder.AlterColumn<double>(
                name: "MaxSumOnPeriod",
                table: "Limit",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "Currency",
                table: "Limit",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LimitNumberCounter",
                table: "Limit",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "LimitSumCounter",
                table: "Limit",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Currency",
                table: "Limit");

            migrationBuilder.DropColumn(
                name: "LimitNumberCounter",
                table: "Limit");

            migrationBuilder.DropColumn(
                name: "LimitSumCounter",
                table: "Limit");

            migrationBuilder.AlterColumn<int>(
                name: "MaxSumOnPeriod",
                table: "Limit",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AddColumn<bool>(
                name: "isPeriodical",
                table: "Limit",
                nullable: false,
                defaultValue: false);
        }
    }
}
