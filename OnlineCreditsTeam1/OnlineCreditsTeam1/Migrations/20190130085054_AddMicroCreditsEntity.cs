﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace OnlineCreditsTeam1.Migrations
{
    public partial class AddMicroCreditsEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MicroCredits",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    BeginDate = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    FinishDate = table.Column<DateTime>(nullable: false),
                    OrderCreationDate = table.Column<DateTime>(nullable: false),
                    OrderTerm = table.Column<int>(nullable: false),
                    Requisite = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    Sum = table.Column<double>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MicroCredits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MicroCredits_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MicroCredits_UserId",
                table: "MicroCredits",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MicroCredits");
        }
    }
}
