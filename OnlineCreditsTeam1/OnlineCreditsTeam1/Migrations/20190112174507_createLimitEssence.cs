﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace OnlineCreditsTeam1.Migrations
{
    public partial class createLimitEssence : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.CreateTable(
                name: "Limit",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CustomLimitBeginDate = table.Column<DateTime>(nullable: false),
                    CustomLimitFinishDate = table.Column<DateTime>(nullable: false),
                    LimitType = table.Column<string>(nullable: true),
                    MaxCreditSprint = table.Column<int>(nullable: false),
                    MaxOrdersOnPeriod = table.Column<int>(nullable: false),
                    MaxSumOnPeriod = table.Column<int>(nullable: false),
                    Period = table.Column<string>(nullable: true),
                    isActive = table.Column<bool>(nullable: false),
                    isPeriodical = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Limit", x => x.Id);
                });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {


            migrationBuilder.DropTable(
                name: "Limit");
        }
    }
}
