﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace OnlineCreditsTeam1.Migrations
{
    public partial class EditLimitSystem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LimitByUser");

            migrationBuilder.DropColumn(
                name: "LimitNumberCounter",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "LimitSumCounter",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "Limit",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Limit_UserId",
                table: "Limit",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Limit_AspNetUsers_UserId",
                table: "Limit",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Limit_AspNetUsers_UserId",
                table: "Limit");

            migrationBuilder.DropIndex(
                name: "IX_Limit_UserId",
                table: "Limit");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Limit");

            migrationBuilder.AddColumn<int>(
                name: "LimitNumberCounter",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "LimitSumCounter",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.CreateTable(
                name: "LimitByUser",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LimitId = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LimitByUser", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LimitByUser_Limit_LimitId",
                        column: x => x.LimitId,
                        principalTable: "Limit",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LimitByUser_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LimitByUser_LimitId",
                table: "LimitByUser",
                column: "LimitId");

            migrationBuilder.CreateIndex(
                name: "IX_LimitByUser_UserId",
                table: "LimitByUser",
                column: "UserId");
        }
    }
}
