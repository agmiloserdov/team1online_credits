﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using OnlineCreditsTeam1.Models;

namespace OnlineCreditsTeam1
{
    public class ApplicationDatabaseInitializer
    {
        public static async Task InitializeAsync(UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            string adminName = "+996123456789";
            string password = "666666";
            
            if (await roleManager.FindByNameAsync("666666") == null)
            {
                await roleManager.CreateAsync(new IdentityRole("admin"));
            }

            if (await roleManager.FindByNameAsync("user") == null)
            {
                await roleManager.CreateAsync(new IdentityRole("user"));
            }
            if (await roleManager.FindByNameAsync("MarketOperator") == null)
            {
                await roleManager.CreateAsync(new IdentityRole("MarketOperator"));
            }
            if (await roleManager.FindByNameAsync("BankOperator") == null)
            {
                await roleManager.CreateAsync(new IdentityRole("BankOperator"));
            }

            if (await userManager.FindByNameAsync(adminName) == null)
            {

                ApplicationUser admin = new ApplicationUser
                {
                    Email = "test@test.ru",
                    UserName = adminName,
                    PhoneNumber = adminName,
                    CreateTime = DateTime.Now,
                    PhotoPath = "/StaticData/Anonymous/admin.jpg",
                    Name = "Admin",
                    Role = "admin"
                };

                

                IdentityResult result = await userManager.CreateAsync(admin, password);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(admin, "admin");
                }
            }
        }
    }
}
