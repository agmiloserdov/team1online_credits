﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OnlineCreditsTeam1.Data;
using OnlineCreditsTeam1.Models;
using X.PagedList;

namespace OnlineCreditsTeam1.Controllers
{
    public class LimitsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public LimitsController(ApplicationDbContext context)
        {
            _context = context;
        }


        // GET: Limits/Details/5
      
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Limit limit, string begindate, string finishdate,List<string> users)
        {
            if (limit.Period == "Day")
            {
                limit.CustomLimitBeginDate = DateTime.Now;
                limit.CustomLimitFinishDate = limit.CustomLimitBeginDate.AddDays(1);
            }
            else if (limit.Period == "Week")
            {
                limit.CustomLimitBeginDate = DateTime.Now;
                limit.CustomLimitFinishDate = limit.CustomLimitBeginDate.AddDays(7);
            }
            else if (limit.Period == "Month")
            {
                limit.CustomLimitBeginDate = DateTime.Now;
                limit.CustomLimitFinishDate = limit.CustomLimitBeginDate.AddMonths(1);
            }
            else if (limit.Period == "Year")
            {
                limit.CustomLimitBeginDate = DateTime.Now;
                limit.CustomLimitFinishDate = limit.CustomLimitBeginDate.AddYears(1);
            }
            else
            {
                limit.CustomLimitBeginDate = DateTime.ParseExact(begindate, "MM/dd/yyyy", null);
                limit.CustomLimitFinishDate = DateTime.ParseExact(finishdate, "MM/dd/yyyy", null);  
            }

            if (limit.MaxCreditSprint == null) limit.MaxCreditSprint = 999999999;
            if (limit.MaxOrdersOnPeriod == null) limit.MaxOrdersOnPeriod = 999999999;
            if (limit.MaxSumOnPeriod == null) limit.MaxSumOnPeriod = 999999999;

            
            if (limit.MaxSumOnPeriod!=null)
            {
                if (!users.Contains("global") && users.Count != 0)
                {
                    foreach (string userId in users)
                    {
                        Limit l = new Limit
                        {
                            LimitNumberCounter = 0,
                            LimitSumCounter = 0,
                            Period = limit.Period,
                            CustomLimitBeginDate = limit.CustomLimitBeginDate,
                            CustomLimitFinishDate = limit.CustomLimitFinishDate,
                            MaxOrdersOnPeriod = limit.MaxOrdersOnPeriod,
                            MaxSumOnPeriod = limit.MaxSumOnPeriod,
                            MaxCreditSprint = limit.MaxCreditSprint,
                            isActive = limit.isActive,
                            Currency = limit.Currency
                        };

                        l.UserId = userId;
                        var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);
                        l.User = user;
                        _context.Limit.Add(l);
                    }
                }
                else
                {
                    limit.LimitNumberCounter = 0;
                    limit.LimitSumCounter = 0;
                    _context.Limit.Add(limit);
                }
                await _context.SaveChangesAsync();
                return RedirectToAction("AdminProfile", "Manage");
            }
            return RedirectToAction("AdminProfile", "Manage");
        }

        public async Task<IActionResult> GetUsers(string keyWord)
        {
            var users = await _context.Users.Where(u => u.Role != "admin" && u.Role != "BankOperator").ToListAsync();
            if (keyWord != null)
            {  
                users = await users.Where(u =>
                    u.UserName.Contains(keyWord) ||
                    u.Role.Contains(keyWord) ||
                    u.Name.Contains(keyWord)).ToListAsync();
                return PartialView("UserSearchResult", users);
            }
            else
            {
                return PartialView("UserSearchResult", users);
            }
        }

        // GET: Limits/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Dictionary<string, string> periods = new Dictionary<string, string>(5)
            {
                { "День", "Day" },
                { "Неделя", "Week" },
                { "Месяц", "Month" },
                { "Год", "Year" },
                { "Произвольный срок", "Custom" }
            };

            ViewBag.PeriodsListByCreate = periods;
            var limit = await _context.Limit.SingleOrDefaultAsync(m => m.Id == id);
            if (limit == null)
            {
                return NotFound();
            }
            return View(limit);
        }

        // POST: Limits/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,  Limit limit, string begindate, string finishdate)
        {

            if (id != limit.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (limit.Period == "Day")
                {
                    limit.CustomLimitFinishDate = limit.CustomLimitBeginDate.AddDays(1);
                }
                else if (limit.Period == "Week")
                {
                    limit.CustomLimitFinishDate = limit.CustomLimitBeginDate.AddDays(7);
                }
                else if (limit.Period == "Month")
                {
                    limit.CustomLimitFinishDate = limit.CustomLimitBeginDate.AddMonths(1);
                }
                else if (limit.Period == "Year")
                {
                    limit.CustomLimitFinishDate = limit.CustomLimitBeginDate.AddYears(1);
                }
                else
                {
                    try
                    {
                        limit.CustomLimitBeginDate = DateTime.ParseExact(begindate, "MM/dd/yyyy", null);
                    }
                    catch (Exception e)
                    {
                        limit.CustomLimitBeginDate = DateTime.Parse(begindate);
                    }

                    try
                    {
                        limit.CustomLimitFinishDate = DateTime.ParseExact(finishdate, "MM/dd/yyyy", null);
                    }
                    catch (Exception e)

                    {
                        limit.CustomLimitFinishDate = DateTime.Parse(finishdate);
                    }    
                }
                

                if (limit.MaxCreditSprint == null) limit.MaxCreditSprint = 999999999;
                if (limit.MaxOrdersOnPeriod == null) limit.MaxOrdersOnPeriod = 999999999;
                if (limit.MaxSumOnPeriod == null) limit.MaxSumOnPeriod = 999999999;
 
                try
                {
                    _context.Update(limit);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LimitExists(limit.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("AdminProfile", "Manage");
            }
            return View(limit);
        }

        // GET: Limits/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var limit = await _context.Limit.Include(m => m.User)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (limit == null)
            {
                return NotFound();
            }

            return View(limit);
        }

        // POST: Limits/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var limit = await _context.Limit.Include(m => m.User).SingleOrDefaultAsync(m => m.Id == id);
            _context.Limit.Remove(limit);
            await _context.SaveChangesAsync();
            return RedirectToAction("AdminProfile", "Manage");
        }

        private bool LimitExists(int id)
        {
            return _context.Limit.Any(e => e.Id == id);
        }


        public async Task<IActionResult> GetLimits()
        {
            var limits = await _context.Limit.OrderBy(l => l.CustomLimitBeginDate).Include(l => l.User).ToListAsync();
            return PartialView("LimitsPartial",limits);
        }
        [HttpGet]
 
        public IActionResult CreateLimit()
        {
            Dictionary<string, string> periods = new Dictionary<string, string>(5)
            {
                { "День", "Day" },
                { "Неделя", "Week" },
                { "Месяц", "Month" },
                { "Год", "Year" },
                { "Произвольный срок", "Custom" }
            };
            ViewBag.PeriodsListByCreate = periods;
            var users = _context.Users.OrderByDescending(u => u.CreateTime).Where(u => u.Role!="admin" && u.Role!="BankOperator");
            ViewBag.Users = users;
            return PartialView("PartialCreateLimit");
        }
    }
}
