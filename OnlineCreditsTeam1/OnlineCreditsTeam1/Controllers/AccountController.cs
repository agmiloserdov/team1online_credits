﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OnlineCreditsTeam1.Data;
using OnlineCreditsTeam1.Extensions;
using OnlineCreditsTeam1.Models;
using OnlineCreditsTeam1.Models.AccountViewModels;
using OnlineCreditsTeam1.Services;
using SQLitePCL;

namespace OnlineCreditsTeam1.Controllers
{
   
    public class AccountController : Controller
    {
        private JsonService _jsonService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger _logger;
        private readonly UserLogWriter _userLogWriter;
        private PasswordGenerator _passwordGenerator;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ApplicationDbContext _context;
        private AccountManager accountManager;

        public AccountController(
            UserLogWriter userLogWriter,
            PasswordGenerator passwordGenerator,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<AccountController> logger,
            RoleManager<IdentityRole> roleManager,
            ApplicationDbContext context,
            JsonService jsonService)
        {
            _userManager = userManager;
            _userLogWriter = userLogWriter;
            _passwordGenerator = passwordGenerator;
            _signInManager = signInManager;
            _logger = logger;
            _roleManager = roleManager;
            _context = context;
            accountManager = new AccountManager(passwordGenerator, userManager, logger);
            _jsonService = jsonService;
        }
        [HttpGet]
        [Authorize(Roles = "admin")]
        public IActionResult CreatureByTheAdminUser()
        {
            var allBankOperators = _context.Users.Where(u => u.Role == "BankOperator").ToList();

            var allRoles = _roleManager.Roles.OrderByDescending(r => r.Name).ToList();
            CreatureByTheAdminUserViewModel model = new CreatureByTheAdminUserViewModel
            {
                AllRoles = allRoles,
                AllBankOperators = allBankOperators
            };
            return PartialView(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreatureByTheAdminUser(CreatureByTheAdminUserViewModel model, string returnUrl = null)
        {
           
            var isUserRegister = await _userManager.Users.FirstOrDefaultAsync(u => u.UserName == model.PhoneNumber);


            if (isUserRegister == null)
            {
                if (ModelState.IsValid)
                {

                    string requisits ="";
                    var user = accountManager.CreateUser(model.PhoneNumber, model.Role, model.Name, "", requisits, model.CreditAdminId);
                    var result = await accountManager.SaveUserAsync(user);
                    LogModel log = new LogModel
                    {
                        ActionDate = DateTime.Now,
                        LogMessage = "Account created by admin",
                        UserId = user.Id
                    };
                    _userLogWriter.WriteLog(log);
                    model.UserId = user.Id;
                    requisits = _jsonService.CreateJson(model);
                    user.Requisits = requisits;
               //     var result1 = await accountManager.SaveUserAsync(user);
                    if (result)
                    {
                        await accountManager.AddToRoleAsync(user, model.Role);
                    }
                    if (model.Role == "user" || model.Role == "MarketOperator")
                    {
                        Limit limit = null;
                        if (model.Role == "user")
                        {
                            limit = _context.Limit.FirstOrDefault(l => l.isActive && l.LimitType == "UsersGlobal" && l.CustomLimitBeginDate < DateTime.Now && l.CustomLimitFinishDate > DateTime.Now);
                        }

                        if (model.Role == "MarketOperator")
                        {
                            limit = _context.Limit.FirstOrDefault(l => l.isActive && l.LimitType == "MarketsGlobal" && l.CustomLimitBeginDate < DateTime.Now && l.CustomLimitFinishDate > DateTime.Now);         
                        }
                        if (limit == null)
                        {
                            limit = _context.Limit.FirstOrDefault(l => l.isActive && l.LimitType == "Global" && l.CustomLimitBeginDate < DateTime.Now && l.CustomLimitFinishDate > DateTime.Now);
                        }


          
                    }
                    await _context.SaveChangesAsync();
                    return RedirectToAction("AdminProfile", "Manage");
                }
            }
            var allRoles = _roleManager.Roles.ToList();
            var allBankOperators = _context.Users.Where(u => u.Role == "BankOperator").ToList();
            model.AllBankOperators = allBankOperators;
            model.AllRoles = allRoles;
            return RedirectToAction("AdminProfile", "Manage", "_CreateUser");
        }


        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }


        [HttpGet]
        public IActionResult CheckUser()
        {
            if (!_signInManager.IsSignedIn(User)) return View();
            if (User.IsInRole("admin"))
            {
                return RedirectToAction("AdminProfile", "Manage");
            }
            else if (User.IsInRole("BankOperator"))
            {
                return RedirectToAction("BankOperatorProfile", "Manage");
            }
            else if (User.IsInRole("MarketOperator"))
            {
                return RedirectToAction("MarketOperatorProfile", "Manage");
            }
            else if (User.IsInRole("user"))
            {
                return RedirectToAction("UserProfile", "Manage");
            }
            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> SendPwd(LoginViewModel model)
        {
            try
            {
                LoginViewModel loginModel = await accountManager.SendPassword(model);

                return View("Login", loginModel);
            }
            catch (Exception e)
            {
                ViewData["StatusMessage"] = $"Ошибка пользователь {model.PhoneNumber} Не найден в системе";
                return View("CheckUser", model);
            }
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
           
            if (ModelState.IsValid)
            {
                if (model.Password == null)
                {
                    return View("CheckUser");
                }
                var result = await _signInManager.PasswordSignInAsync(model.PhoneNumber,  model.Password, false, lockoutOnFailure: true);
                if (result.Succeeded)
                {
                    _logger.LogInformation("User logged in.");

                    return RedirectToAction("CheckUser");
                }

                if (result.IsLockedOut)
                {
                    _logger.LogWarning("User account locked out.");
                    return RedirectToAction(nameof(Lockout));
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return RedirectToAction("CheckUser");
                }
            }
            return View("CheckUser", model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Lockout()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }
        


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                
                var user = new ApplicationUser { UserName = model.PhoneNumber, PhoneNumber = model.PhoneNumber, Name= model.Name, Email = "",  Requisits = null, CreateTime = DateTime.Now, Role = "user" };
                
                string password = _passwordGenerator.GeneratePassword(6);
                var result = await _userManager.CreateAsync(user, password);
                if (result.Succeeded)
                {
                    LoginViewModel loginModel = new LoginViewModel
                        {PhoneNumber = model.PhoneNumber, Password = password};
                    await _userManager.AddToRoleAsync(user, "user");
                    return View("Login",loginModel);
                }
                AddErrors(result);
            }
            return View(model);
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation("User logged out.");
            return RedirectToAction("CheckUser", "Account");
        }


        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
    }
}
