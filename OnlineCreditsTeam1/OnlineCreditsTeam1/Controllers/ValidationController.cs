﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace OnlineCreditsTeam1.Controllers
{
    public class ValidationController : Controller
    {
        public static DateTime beginDate = new DateTime();
        [AcceptVerbs("Get")]
        public IActionResult ValidateMaxOrdersOnPeriod(int MaxOrdersOnPeriod)
        {
            if (MaxOrdersOnPeriod < 0 || MaxOrdersOnPeriod > 9999)
                return Json(false);

            return Json(true);
        }
        [AcceptVerbs("Get")]
        public IActionResult ValidateMaxSumOnPeriod(int MaxSumOnPeriod)
        {
            if (MaxSumOnPeriod < 0 || MaxSumOnPeriod > 99999999)
                return Json(false);

            return Json(true);
        }
        [AcceptVerbs("Get")]
        public IActionResult ValidateMaxCreditSprint(int MaxCreditSprint)
        {
            if (MaxCreditSprint < 0 || MaxCreditSprint > 24)
                return Json(false);

            return Json(true);
        }

       
    }
}