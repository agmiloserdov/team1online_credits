﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineCreditsTeam1.Data;
using OnlineCreditsTeam1.Models;
using OnlineCreditsTeam1.Models.ManageViewModels;
using OnlineCreditsTeam1.Services;

namespace OnlineCreditsTeam1.Controllers
{
    [Authorize]
    public class MicroCreditController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserLogWriter _logWriter;

        public MicroCreditController(ApplicationDbContext context, UserLogWriter logWriter)
        {
            _context = context;
            _logWriter = logWriter;
        }

        [HttpPost]
        public IActionResult CreateCredit(UserProfileViewModel model)
        {
            MicroCredit credit = new MicroCredit
            {
                BeginDate = model.Credit.BeginDate,
                Currency = model.Credit.Currency,
                Description = model.Credit.Description,
                FinishDate = model.Credit.FinishDate,
                Requisite = model.Credit.Requisite,
                Sum = model.Credit.Sum,
                UserId = model.Credit.UserId,
                Status = "new",
                OrderTerm = (model.Credit.FinishDate - DateTime.Now).Days/30
            };
            _context.MicroCredits.Add(credit);
            _context.SaveChanges();
            _logWriter.WriteLog(new LogModel
            {
                ActionDate = DateTime.Now,
                LogMessage = $"Пользователь создал заявку на получение микрокредита {credit.Sum} {credit.Currency}",
                UserId = credit.UserId
            });
            return RedirectToAction("UserProfile", "Manage");
        }

        [HttpGet]
        public IActionResult GetUserCredits(string userId)
        {
            List<MicroCredit> credits = new List<MicroCredit>();
            try
            {
                credits = _context.MicroCredits.Where(credit => credit.UserId == userId).ToList();
            }
            catch (Exception e)
            {

            }
            return PartialView("CreditListPartial", credits);
        }
    }
}
