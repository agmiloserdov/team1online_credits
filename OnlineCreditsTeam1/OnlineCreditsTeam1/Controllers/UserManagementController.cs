﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using OnlineCreditsTeam1.Data;
using OnlineCreditsTeam1.Models;
using OnlineCreditsTeam1.Models.ManageViewModels;
using OnlineCreditsTeam1.Services;
using X.PagedList;

namespace OnlineCreditsTeam1.Controllers
{
    [Authorize]
    public class UserManagementController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private PasswordGenerator _passwordGenerator;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ApplicationDbContext _context;
        private const int pageSize = 4;
        private readonly FileUploadService _fileUploadService;
        private readonly IHostingEnvironment _environment;
        private readonly IStringLocalizer<UserManagementController> _localizer;
        private JsonService _jsonService;

        public UserManagementController(
            PasswordGenerator passwordGenerator,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ILogger<AccountController> logger,
            RoleManager<IdentityRole> roleManager,
            ApplicationDbContext context,
            FileUploadService fileUploadService,
            IHostingEnvironment environment,
            IStringLocalizer<UserManagementController> localizer,
            JsonService jsonService
            )
        {
            _userManager = userManager;
            _passwordGenerator = passwordGenerator;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
            _roleManager = roleManager;
            _context = context;
            _fileUploadService = fileUploadService;
            _environment = environment;
            _localizer = localizer;
            _jsonService = jsonService;
        }


        public enum SortState
        {
            NameAsc,
            NameDesc,
            DateAsc,
            DateDesc,
            UserNameAsc,
            UserNameDesc,
            StatusAsc,
            StatusDesc,
            RoleAsc,
            RoleDesc
        }

        // GET: UserManagement
        public async Task<IActionResult> UserListForAdmin(string userName, SortState sortOrder = SortState.NameAsc, int page = 1)
        {

            ViewBag.UserNameFilter = userName;
            var users = _context.Users.Where(u=> u.UserName !=null);

            ViewBag.CurrentSort = sortOrder;
            if (!string.IsNullOrWhiteSpace(userName))
            {
                users = users.Where(u => u.UserName.Contains(userName)||u.Name.Contains(userName));
            }

            
            ViewBag.NameSort = sortOrder == SortState.NameAsc ? SortState.NameDesc : SortState.NameAsc;
            ViewBag.DateSort = sortOrder == SortState.DateAsc ? SortState.DateDesc : SortState.DateAsc;
            ViewBag.UserNameSort = sortOrder == SortState.UserNameAsc ? SortState.UserNameDesc : SortState.UserNameAsc;
            ViewBag.RoleSort = sortOrder == SortState.RoleAsc ? SortState.RoleDesc : SortState.RoleAsc;
            ViewBag.StatusSort = sortOrder == SortState.StatusAsc ? SortState.StatusDesc : SortState.StatusAsc;

            switch (sortOrder)
            {
                case SortState.NameDesc:
                    users = users.OrderByDescending(s => s.Name);
                    break;
                case SortState.DateAsc:
                    users = users.OrderBy(s => s.CreateTime);
                    break;
                case SortState.DateDesc:
                    users = users.OrderByDescending(s => s.CreateTime);
                    break;
                case SortState.UserNameAsc:
                    users = users.OrderBy(s => s.UserName);
                    break;
                case SortState.UserNameDesc:
                    users = users.OrderByDescending(s => s.UserName);
                    break;
                case SortState.StatusAsc:
                    users = users.OrderBy(s => s.Status);
                    break;
                case SortState.StatusDesc:
                    users = users.OrderByDescending(s => s.Status);
                    break;
                case SortState.RoleAsc:
                    users = users.OrderBy(s => s.Role);
                    break;
                case SortState.RoleDesc:
                    users = users.OrderByDescending(s => s.Role);
                    break;
                default:
                    users = users.OrderBy(s => s.Name);
                    break;
            }


            IPagedList<ApplicationUser> usersForPages = users.ToPagedList(page, pageSize);
            return View(usersForPages);
        }

        public async Task<IActionResult> GetUsersByShopId(string shopId)
        {
            List<Order> orders = new List<Order>();
            try
            {
                orders = await _context.Order.Where(o => o.CorpClientId == shopId).ToListAsync();
            }
            catch (Exception e)
            {
                return PartialView("UsersPartial");
            }
            
            List<ApplicationUser> users = new List<ApplicationUser>();
            foreach (var order in orders)
            {
               users.Add(_userManager.Users.FirstOrDefault(u => u.Id == order.ClientId)); 
            }
            return PartialView("UsersPartial", users);
        }

        [HttpGet]
        public async Task<IActionResult> GetOrdersByShopId(string shopId)
        {
            SettingsService settingsService = new SettingsService();
            Order myOrder = new Order
            {
                BeginDate = DateTime.Now,
                FinishDate = DateTime.MaxValue,
                Id = 0,
                ClientId = "test_Client_Id",
                Sum = 0,
                Status = "Test",
                CorpClientId = "test_Corporative_Id"
            };
            List<Order> orders = new List<Order>();
            try
            {
                orders = await _context.Order.Where(o => o.CorpClientId == shopId).ToListAsync();
                List<Order> newOrders = await orders.Where(o => o.Status == "new").ToListAsync();
                if (newOrders.Count > 0)
                {
                    foreach (Order order in newOrders)
                    {
                        var documents = await _context.Documents.Where(o => o.OrderId == order.Id).ToListAsync();
                        if (documents.Count < 4 &&
                            DateTime.Now >= order.BeginDate.AddDays(settingsService.GetSettnigs().AutoCancelOrder))
                        {
                            order.Status = "Отклонена";
                            _context.Update(order);
                        }
                    }
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                orders.Add(myOrder);
            }

            return PartialView("_OrdersPartial", orders);
        }

        [HttpGet]
        public async Task<IActionResult> GetOrdersById(string userId)
        {
            SettingsService settingsService = new SettingsService();
            Order myOrder = new Order
            {
                BeginDate = DateTime.Now,
                FinishDate = DateTime.MaxValue,
                Id = 0,
                ClientId = "test_Client_Id",
                Sum = 0,
                Status = "Test",
                CorpClientId = "test_Corporative_Id"
            };
            List<Order> orders = new List<Order>();
            try
            {
                orders = await _context.Order.Where(o => o.ClientId == userId).ToListAsync();
                List<Order> newOrders = await orders.Where(o => o.Status == "new").ToListAsync();
                if (newOrders.Count > 0)
                {
                    foreach (Order order in newOrders)
                    {
                        var documents = await _context.Documents.Where(o => o.OrderId == order.Id).ToListAsync();
                        if (documents.Count < 4 &&
                            DateTime.Now >= order.BeginDate.AddDays(settingsService.GetSettnigs().AutoCancelOrder))
                        {
                            order.Status = "Отклонена";
                            _context.Update(order);
                        }
                    }
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                orders.Add(myOrder);
            }
            
            
           // var applicationDbContext = _context.Order.Include(a => a.ClientId).Include(a => a.BeginDate).OrderByDescending(p => p.Status).ToList();
            return PartialView("_OrdersPartial", orders);
        }

        [HttpGet]
        public async Task<IActionResult> GetOrders()
        {
            SettingsService settingsService = new SettingsService();
            Order myOrder = new Order
            {
                BeginDate = DateTime.Now,
                FinishDate = DateTime.MaxValue,
                Id = 0,
                ClientId = "test_Client_Id",
                Sum = 0,
                Status = "Test",
                CorpClientId = "test_Corporative_Id"
            };
            List<Order> orders = new List<Order>();
            
            try
            {
                string currentUserId = _userManager.GetUserId(User);
                if (User.IsInRole("admin"))
                {
                    orders = _context.Order.OrderBy(o => o.BeginDate).ToList();
                }
                else if (User.IsInRole("MarketOperator"))
                {
                    orders = _context.Order.OrderBy(o => o.BeginDate).Where(order => order.CorpClientId == currentUserId)
                        .ToList();
                }
                else if (User.IsInRole("BankOperator"))
                {
                    List<ApplicationUser> users = _context.Users.Where(user => user.CreditAdminId.Equals(currentUserId))
                        .ToList();
                    foreach (ApplicationUser u in users)
                    {
                        orders.AddRange(await _context.Order.Where(order => order.ClientId == u.Id).ToListAsync());
                    }

                    orders.OrderBy(o => o.BeginDate);
                }
                else if (User.IsInRole("user"))
                {
                    orders = await _context.Order.OrderBy(o => o.BeginDate)
                        .Where(order => order.ClientId == currentUserId).ToListAsync();
                }
            }
            catch (Exception e)
            {
                orders.Add(myOrder);
            }
            List<Order> newOrders = await orders.Where(o => o.Status == "new").ToListAsync();
            if (newOrders.Count > 0)
            {
                foreach (Order order in newOrders)
                {
                    var documents = await _context.Documents.Where(o => o.OrderId == order.Id).ToListAsync();
                    if (documents.Count < 4 &&
                        DateTime.Now >= order.BeginDate.AddDays(settingsService.GetSettnigs().AutoCancelOrder))
                    {
                        order.Status = "Отклонена";
                        _context.Update(order);
                    }
                }
                await _context.SaveChangesAsync();
            }
           
            return PartialView("_OrdersPartial", orders);
        }

        [HttpGet]
        public async Task<IActionResult> UserDetails(string id)
        {
            UserProfileViewModel model = new UserProfileViewModel();
            var user = await _userManager.FindByIdAsync(id);
            if (user.Role == "user")
            {
               model = new UserProfileViewModel()
                {
                    UserName = user.UserName,
                    Name = user.Name,
                    Email = user.Email,
                    CreateDate = user.CreateTime,
                    PhotoPath = user.PhotoPath,
                    Id = user.Id,
                    Status = user.Status,
                    Role = user.Role
               };
            }
            else
            {
                model = new UserProfileViewModel()
                {
                    UserName = user.UserName,
                    Name = user.Name,
                    Email = user.Email,
                    CreateDate = user.CreateTime,
                    PhotoPath = user.PhotoPath,
                    Id = user.Id,
                    Status = user.Status,
                    Requisits = _jsonService.GetJson(user.Requisits),
                    Role = user.Role
                };

            }

            ViewData["GetReportModel"] = new GetReportModel(){UserId = user.Id};
          
            return View(model);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UserDetails(string id, UserProfileViewModel userModel)
        {
            var user = await _userManager.FindByIdAsync(id);
            try
            {
                
                string pathPhoto = $"{_environment.WebRootPath}/DynamicsData/{user.Id}/";
                ViewData["GetReportModel"] = new GetReportModel() { UserId = user.Id };
                SettingsService service = new SettingsService();
                long size = (long)service.GetSettnigs().FileSize;

                var userRoles = await _userManager.GetRolesAsync(user);
                ViewBag.RolesList = userRoles;

                string photoFile;
                if (userModel.PhotoFile?.FileName == null || size < userModel.PhotoFile?.Length / 1024)
                {
                    photoFile = user.PhotoPath;
                    if (size < userModel.PhotoFile?.Length / 1024)
                    {
                        ViewData["StatusMessage"] = $"Размер файла больше допустимого({size}kb)";
                        userModel.CreateDate = user.CreateTime;
                        userModel.PhotoPath = user.PhotoPath;
                        userModel.Status = user.Status;
                        return View(userModel);
                    }
                }
                else
                {
                    photoFile = $"/DynamicsData/{user.Id}/{userModel.PhotoFile.FileName}";
                    string pattern = @"^.*\.(jpg|JPG|jpeg|png)$";
                    if (Regex.IsMatch(photoFile, pattern, RegexOptions.IgnoreCase))
                    {
                        _fileUploadService.Upload(pathPhoto, userModel.PhotoFile.FileName, userModel.PhotoFile);
                    }
                    else
                    {
                        ViewData["StatusMessage"] = "Недопустимый формат файла";
                        userModel.CreateDate = user.CreateTime;
                        userModel.PhotoPath = user.PhotoPath;
                        userModel.Status = user.Status;
                        return View(userModel);
                    }
                }
                var email = await _userManager.GetEmailAsync(user);
                if (userModel.Email != email)
                {
                    var setEmailResult = await _userManager.SetEmailAsync(user, userModel.Email);
                    if (!setEmailResult.Succeeded)
                    {

                        throw new InvalidOperationException($"Unexpected error occurred setting email for user with ID '{id}'.");
                    }
                }
                var userName = await _userManager.GetUserNameAsync(user);
                if (userModel.UserName != userName)
                {
                    var setUserNameResult = await _userManager.SetUserNameAsync(user, userModel.UserName);
                    var setUserPhoneResult = await _userManager.SetPhoneNumberAsync(user, userModel.UserName);
                    if (!setUserNameResult.Succeeded)
                    {

                        throw new InvalidOperationException($"Unexpected error occurred setting username for user with ID '{id}'.");
                    }
                    if (!setUserPhoneResult.Succeeded)
                    {

                        throw new InvalidOperationException($"Unexpected error occurred setting phonenumber for user with ID '{id}'.");
                    }
                }

                user.PhotoPath = photoFile;
                user.Name = userModel.Name;
                ViewData["StatusMessage"] = _localizer["success_profile_has_been_updated"].Value;
                await _userManager.UpdateAsync(user);
                userModel.CreateDate = user.CreateTime;
                userModel.PhotoPath = user.PhotoPath;
                userModel.Status = user.Status;
                return View(userModel);
            }

           catch (Exception e)
            {
                userModel.CreateDate = user.CreateTime;
                userModel.PhotoPath = user.PhotoPath;
                userModel.Status = user.Status;


                return View(userModel);
            }
            
        }

        [HttpGet]
        public async Task<IActionResult> GetUserByRole(string role)
        {
            string currentUserId = _userManager.GetUserId(User);
            List<ApplicationUser> users = new List<ApplicationUser>();
            
            if (User.IsInRole("BankOperator"))
            {
                users = await _userManager.Users.Where(u => u.Role == role && u.CreditAdminId == currentUserId).ToListAsync();
            }
            else
            {
                users = await _userManager.Users.Where(u => u.Role == role).ToListAsync();
            }
            return PartialView("UsersPartial", users);
        }


    }
}