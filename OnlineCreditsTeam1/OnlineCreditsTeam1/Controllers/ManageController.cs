﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OnlineCreditsTeam1.Data;
using OnlineCreditsTeam1.Models;
using OnlineCreditsTeam1.Models.AccountViewModels;
using OnlineCreditsTeam1.Models.ManageViewModels;
using OnlineCreditsTeam1.Models.SettingsViewModels;
using OnlineCreditsTeam1.Services;

namespace OnlineCreditsTeam1.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class ManageController : Controller
    {
        private JsonService _jsonService;
        private readonly ApplicationDbContext _context;
        private readonly FileUploadService _fileUploadService;
        private readonly IHostingEnvironment _environment;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserLogWriter _userLogWriter;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserActionLogger _actionLogger;
        private readonly IStringLocalizer<ManageController> _localizer;

        public ManageController(ApplicationDbContext context,
       
            FileUploadService fileUploadService,
            UserLogWriter userLogWriter,
        IHostingEnvironment environment,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            RoleManager<IdentityRole> roleManager,
            UserActionLogger actionLogger,
            IStringLocalizer<ManageController> localizer,
            JsonService jsonService)
        {
            _userManager = userManager;
            _userLogWriter = userLogWriter;
            _actionLogger = actionLogger;
            _signInManager = signInManager;
            _environment = environment;
            _context = context;
            _fileUploadService = fileUploadService;
            _roleManager = roleManager;
            _localizer = localizer;
            _jsonService = jsonService;
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> AdminProfile()
        {

            var user = await _userManager.GetUserAsync(User);
            var userRoles = await _userManager.GetRolesAsync(user);
            ViewBag.RolesList = userRoles;
            var userList = await _context.Users.Where(u => u.Role == "MarketOperator" || u.Role == "BankOperator")
                .ToListAsync();
            ViewBag.UserList = userList;
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AdminProfile(UserProfileViewModel model)
        {
            SettingsService service = new SettingsService();
            long size = (long)service.GetSettnigs().FileSize;

            if (ModelState.IsValid)
            {
                var user = await _userManager.GetUserAsync(User);
                if (user == null)
                {
                    return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
                }
                string pathPhoto = $"{_environment.WebRootPath}/DynamicsData/{user.Id}/";

                var userRoles = await _userManager.GetRolesAsync(user);
                ViewBag.RolesList = userRoles;
 
                string photoFile;
                if (model.PhotoFile?.FileName == null || size < model.PhotoFile?.Length / 1024)
                {
                    photoFile = user.PhotoPath;
                    if (size < model.PhotoFile?.Length / 1024)
                        ViewData["StatusMessage"] = $"Размер файла больше допустимого({size}kb)";
                }
                else
                {
                   
                    photoFile = $"/DynamicsData/{user.Id}/{model.PhotoFile.FileName}";
                    string pattern = @"^.*\.(jpg|JPG|jpeg|png)$";
                    if (Regex.IsMatch(photoFile, pattern, RegexOptions.IgnoreCase))
                    {
                        _fileUploadService.Upload(pathPhoto, model.PhotoFile.FileName, model.PhotoFile);
                        ViewData["StatusMessage"] = _localizer["success_profile_has_been_updated"].Value;
                    }
                    else
                    {
                        ViewData["StatusMessage"] = "Недопустимый формат файла";
                    }
                  
                }


                var email = await _userManager.GetEmailAsync(user);
                if (model.Email != email)
                {
                    var setEmailResult = await _userManager.SetEmailAsync(user, model.Email);
                    if (!setEmailResult.Succeeded)
                    {
                        var userId = await _userManager.GetUserIdAsync(user);
                        throw new InvalidOperationException($"Unexpected error occurred setting email for user with ID '{userId}'.");
                    }
                    ViewData["StatusMessage"] = _localizer["success_profile_has_been_updated"].Value;
                }
                user.PhotoPath = photoFile;
                

                await _signInManager.RefreshSignInAsync(user);
                await _userManager.UpdateAsync(user);
            }
            var userList = await _context.Users.Where(u => u.Role == "MarketOperator" || u.Role == "BankOperator")
                .ToListAsync();
            ViewBag.UserList = userList;
            return View(model);
        }

        [HttpGet]
        [Authorize(Roles = "BankOperator")]
        public async Task<IActionResult> BankOperatorProfile()
        {
            var user = await _userManager.GetUserAsync(User);
            var userRoles = await _userManager.GetRolesAsync(user);
            ViewBag.RolesList = userRoles;
            ViewBag.Inn = _jsonService.GetJson(user.Requisits).Inn;
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "BankOperator")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> BankOperatorProfile(UserProfileViewModel model)
        {
            SettingsService service = new SettingsService();
            long size = (long)service.GetSettnigs().FileSize;

            if (ModelState.IsValid)
            {
                var user = await _userManager.GetUserAsync(User);
                if (user == null)
                {
                    return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
                }
                string pathPhoto = $"{_environment.WebRootPath}/DynamicsData/{user.Id}/";

                var userRoles = await _userManager.GetRolesAsync(user);
                ViewBag.RolesList = userRoles;


                string photoFile;
                if (model.PhotoFile?.FileName == null || size < model.PhotoFile?.Length / 1024)
                {
                    photoFile = user.PhotoPath;
                    if (size < model.PhotoFile?.Length / 1024)
                        ViewData["StatusMessage"] = $"Размер файла больше допустимого({size}kb)";
                    return View(model);
                }
                else
                {
                    photoFile = $"/DynamicsData/{user.Id}/{model.PhotoFile.FileName}";
                    string pattern = @"^.*\.(jpg|JPG|jpeg|png)$";
                    if (Regex.IsMatch(photoFile, pattern, RegexOptions.IgnoreCase))
                    {
                        _fileUploadService.Upload(pathPhoto, model.PhotoFile.FileName, model.PhotoFile);
                        LogModel log = new LogModel
                        {
                            ActionDate = DateTime.Now,
                            LogMessage = $"User {user.Name} upload new profile photo {model.PhotoFile.FileName}",
                            UserId = user.Id
                        };
                        _userLogWriter.WriteLog(log);
                        ViewData["StatusMessage"] = _localizer["success_profile_has_been_updated"].Value;
                    }
                    else
                    {
                        ViewData["StatusMessage"] = "Недопустимый формат файла";
                        return View(model);
                    }
                }


                var email = await _userManager.GetEmailAsync(user);
                if (model.Email != email)
                {
                    var setEmailResult = await _userManager.SetEmailAsync(user, model.Email);
                    
                    LogModel log = new LogModel
                    {
                        ActionDate = DateTime.Now,
                        LogMessage = $"User {user.Name} change email to {model.Email}",
                        UserId = user.Id
                    };
                    _userLogWriter.WriteLog(log);

                    if (!setEmailResult.Succeeded)
                    {
                        var userId = await _userManager.GetUserIdAsync(user);
                        throw new InvalidOperationException($"Unexpected error occurred setting email for user with ID '{userId}'.");
                    }
                    ViewData["StatusMessage"] = _localizer["success_profile_has_been_updated"].Value;
                }
                user.PhotoPath = photoFile;
                

                await _signInManager.RefreshSignInAsync(user);
                await _userManager.UpdateAsync(user);
            }
            return View(model);
        }

        [HttpGet]
        [Authorize(Roles = "MarketOperator")]
        public async Task<IActionResult> MarketOperatorProfile()
        {
            var user = await _userManager.GetUserAsync(User);
            var userRoles = await _userManager.GetRolesAsync(user);
            ViewBag.RolesList = userRoles;
            ViewBag.BankAccountNumber = _jsonService.GetJson(user.Requisits).BankAccountNumber;
            ViewBag.Bik = _jsonService.GetJson(user.Requisits).Bik;
            ViewBag.LegalAdress = _jsonService.GetJson(user.Requisits).LegalAdress;
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "MarketOperator")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> MarketOperatorProfile(UserProfileViewModel model)
        {
            SettingsService service = new SettingsService();
            long size = (long)service.GetSettnigs().FileSize;
            
            if (ModelState.IsValid)
            {
                var user = await _userManager.GetUserAsync(User);
                if (user == null)
                {
                    return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
                }
                string pathPhoto = $"{_environment.WebRootPath}/DynamicsData/{user.Id}/";

                var userRoles = await _userManager.GetRolesAsync(user);
                ViewBag.RolesList = userRoles;

                string photoFile;
                if (model.PhotoFile?.FileName == null || size < model.PhotoFile?.Length / 1024)
                {
                    photoFile = user.PhotoPath;
                    if (size < model.PhotoFile?.Length / 1024)
                        ViewData["StatusMessage"] = $"Размер файла больше допустимого({size}kb)";
                    return View(model);
                }
                else
                {
                    photoFile = $"/DynamicsData/{user.Id}/{model.PhotoFile.FileName}";
                    string pattern = @"^.*\.(jpg|JPG|jpeg|png)$";
                    if (Regex.IsMatch(photoFile, pattern, RegexOptions.IgnoreCase))
                    {
                        _fileUploadService.Upload(pathPhoto, model.PhotoFile.FileName, model.PhotoFile);
                        LogModel log = new LogModel
                        {
                            ActionDate = DateTime.Now,
                            LogMessage = $"User {user.Name} upload new profile photo {model.PhotoFile.FileName}",
                            UserId = user.Id
                        };
                        _userLogWriter.WriteLog(log);
                        ViewData["StatusMessage"] = _localizer["success_profile_has_been_updated"].Value;
                    }
                    else
                    {
                        ViewData["StatusMessage"] = "Недопустимый формат файла";
                        return View(model);
                    }
                }


                var email = await _userManager.GetEmailAsync(user);
                if (model.Email != email)
                {
                    var setEmailResult = await _userManager.SetEmailAsync(user, model.Email);
                    LogModel log = new LogModel
                    {
                        ActionDate = DateTime.Now,
                        LogMessage = $"User {user.Name} change email to {model.Email}",
                        UserId = user.Id
                    };
                    _userLogWriter.WriteLog(log);
                    if (!setEmailResult.Succeeded)
                    {
                        var userId = await _userManager.GetUserIdAsync(user);
                        throw new InvalidOperationException($"Unexpected error occurred setting email for user with ID '{userId}'.");
                    }
                    ViewData["StatusMessage"] = _localizer["success_profile_has_been_updated"].Value;
                }
                user.PhotoPath = photoFile;
                

                await _signInManager.RefreshSignInAsync(user);
                await _userManager.UpdateAsync(user);
            }
            return View(model);
        }
        // GET: Manage/UserProfile
        [HttpGet]
        [Authorize(Roles = "user")]
        public async Task<IActionResult> UserProfile()
        {
            var user = await _userManager.GetUserAsync(User);
            UserProfileViewModel model = new UserProfileViewModel();
            var userRoles = await _userManager.GetRolesAsync(user);
            ViewBag.RolesList = userRoles;
            ViewBag.UserId = _userManager.GetUserAsync(User).Result.Id;
            return View(model);

        }


        /// <summary>
        /// Get метод для получения списка настроек
        /// </summary>
        /// <returns>Возвращает список настроек</returns>
        [HttpGet]
        public async Task<IActionResult> Settings()
        {
            SettingsService settings = new SettingsService();
            
            return PartialView(settings.GetSettnigs());
        }

        /// <summary>
        /// Post метод для изменения настроек
        /// </summary>
        /// <returns>Возвращает список настроек</returns>
        [HttpPost]
        public async Task<IActionResult> Settings(SettingViewModel settingList)
        {
            SettingsService settings = new SettingsService();
            try
            {
                settings.SaveSettings(settingList);
                ViewData["StatusMessage"] = "Success. Your profile has been updated";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                ViewData["StatusMessage"] = "Error. Your profile has been updated";
            }
           
            return RedirectToAction("AdminProfile");
        }


        /// <summary>
        ///  Метод для получения пользовательских логов
        /// </summary>
        /// <param name="userId">id пользователя для получения логов</param>
        /// <returns>Возвращает логи пользователя</returns>
        public async Task<IActionResult> GetUserLogs(string userId)
        {
            LogViewModel model = await _actionLogger.LoadUserLog(userId);
            return PartialView("UserLogsPartial", model);
        }
        public async Task<IActionResult> GetAllLogs()
        {
            LogViewModel model = await _actionLogger.LoadUserLog();
            return PartialView("UserLogsPartial", model);
        }

        // POST: Manage/UserProfile
        [HttpPost]
        [Authorize(Roles = "user")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UserProfile(UserProfileViewModel model)
        {
            SettingsService service = new SettingsService();
            long size = (long)service.GetSettnigs().FileSize;

            ViewBag.UserId = _userManager.GetUserAsync(User).Result.Id;
            if (ModelState.IsValid)
            {
                var user = await _userManager.GetUserAsync(User);
                if (user == null)
                {
                    return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
                }
                string pathPhoto = $"{_environment.WebRootPath}/DynamicsData/{user.Id}/";

                var userRoles = await _userManager.GetRolesAsync(user);
                ViewBag.RolesList = userRoles;

                string photoFile;
                if (model.PhotoFile?.FileName == null || size < model.PhotoFile?.Length / 1024)
                {
                    photoFile = user.PhotoPath;
                    if (size < model.PhotoFile?.Length / 1024)
                        ViewData["StatusMessage"] = $"Размер файла больше допустимого({size}kb)";
                    return View(model);
                }
                else
                {
                    photoFile = $"/DynamicsData/{user.Id}/{model.PhotoFile.FileName}";
                    string pattern = @"^.*\.(jpg|JPG|jpeg|png)$";
                    if (Regex.IsMatch(photoFile, pattern, RegexOptions.IgnoreCase))
                    {
                        _fileUploadService.Upload(pathPhoto, model.PhotoFile.FileName, model.PhotoFile);
                        LogModel log = new LogModel
                        {
                            ActionDate = DateTime.Now,
                            LogMessage = $"User {user.Name} upload new photo {model.PhotoFile.FileName}",
                            UserId = user.Id
                        };
                        _userLogWriter.WriteLog(log);
                        ViewData["StatusMessage"] = _localizer["success_profile_has_been_updated"].Value;
                    }
                    else
                    {
                        ViewData["StatusMessage"] = "Недопустимый формат файла";
                        return View(model);
                    }
                }

                var email = await _userManager.GetEmailAsync(user);
                if (model.Email != email)
                {
                    var setEmailResult = await _userManager.SetEmailAsync(user, model.Email);
                    LogModel log = new LogModel
                    {
                        ActionDate = DateTime.Now,
                        LogMessage = $"User {user.Name} change email to {model.Email}",
                        UserId = user.Id
                    };
                    _userLogWriter.WriteLog(log);
                    if (!setEmailResult.Succeeded)
                    {
                        var userId = await _userManager.GetUserIdAsync(user);
                        throw new InvalidOperationException($"Unexpected error occurred setting email for user with ID '{userId}'.");
                    }
                    ViewData["StatusMessage"] = _localizer["success_profile_has_been_updated"].Value;
                }


                _context.SaveChanges();

               
                user.PhotoPath = photoFile;


                

                await _signInManager.RefreshSignInAsync(user);
                await _userManager.UpdateAsync(user);
            }
            return View(model);
        }

    }
}
