﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OnlineCreditsTeam1.Models;
using OnlineCreditsTeam1.ViewModels;

namespace OnlineCreditsTeam1.Controllers
{
    [Authorize(Roles = "admin")]
    public class RolesController : Controller
    {

        RoleManager<IdentityRole> _roleManager;
        UserManager<ApplicationUser> _userManager;
        public RolesController(RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }
        public IActionResult Index() => View(_roleManager.Roles.ToList());

        public IActionResult Create() => View();
        [HttpPost]
        public async Task<IActionResult> Create(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                IdentityResult result = await _roleManager.CreateAsync(new IdentityRole(name));
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View(name);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            IdentityRole role = await _roleManager.FindByIdAsync(id);
            if (role != null)
            {
                IdentityResult result = await _roleManager.DeleteAsync(role);
            }
            return RedirectToAction("Index");
        }

        public IActionResult UserList() => View(_userManager.Users.ToList());

        public async Task<IActionResult> Edit(string userId)
        {

            ApplicationUser user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {

                var userRoles = await _userManager.GetRolesAsync(user);
                var allRoles = _roleManager.Roles.ToList();
                ChangeRoleViewModel model = new ChangeRoleViewModel
                {
                    UserId = user.Id,
                    UserEmail = user.Email,
                    UserRoles = userRoles,
                    AllRoles = allRoles
                };
              

                return View(model);
            }

            return NotFound();
        }
        [HttpPost]
        public async Task<IActionResult> Edit(string userId, List<string> roles)
        {

            ApplicationUser user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {

                var userRoles = await _userManager.GetRolesAsync(user);
               

                var allRoles = _roleManager.Roles.ToList();

                var addedRoles = roles.Except(userRoles);
                
                if (roles.Contains("MarketOperator") && !user.PhotoPath.Contains(userId)) 
                {
                    user.PhotoPath = "/StaticData/Anonymous/Market.jpg";
                }
                if (roles.Contains("BankOperator") && !user.PhotoPath.Contains(userId))
                {
                    user.PhotoPath = "/StaticData/Anonymous/Bank.jpg";
                }
                if (roles.Contains("user") && !user.PhotoPath.Contains(userId))
                {
                    user.PhotoPath = "/StaticData/Anonymous/notimageuser.jpg";
                }
                var removedRoles = userRoles.Except(roles);

                await _userManager.AddToRolesAsync(user, addedRoles);
                user.Role = addedRoles.ToList()[0];
                await _userManager.UpdateAsync(user);

                await _userManager.RemoveFromRolesAsync(user, removedRoles);


                return RedirectToAction("AdminProfile", "Manage");
            }

            return NotFound();
        }
        [HttpPost]
        public async Task<IActionResult> Locked(string userId)
        {

            ApplicationUser user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                await _userManager.SetLockoutEnabledAsync(user, true);
                await _userManager.SetLockoutEndDateAsync(user, new DateTimeOffset(new DateTime(2020, 11, 11)));
                user.Status = "Заблокирован";
            }

            await _userManager.UpdateAsync(user);
            
            return View(user);
        }
        [HttpPost]
        public async Task<IActionResult> UnLocked(string userId)
        {

            ApplicationUser user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                await _userManager.SetLockoutEnabledAsync(user, false);
                user.Status = "Активен";
            }

            await _userManager.UpdateAsync(user);
            return View(user);
        }
    }
}