﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineCreditsTeam1.Data;
using OnlineCreditsTeam1.Models;
using OnlineCreditsTeam1.Services;

namespace OnlineCreditsTeam1.Controllers
{
    [Authorize]
    public class ReportController : Controller
    {
        private ReportGenerator _reportGenerator;
        private readonly ApplicationDbContext _context;

        public ReportController(ReportGenerator reportGenerator, ApplicationDbContext context)
        {
            _reportGenerator = reportGenerator;
            _context = context;
        }

        public IActionResult GetReport(GetReportModel model)
        {
            List<Order> orders = new List<Order>();
            try
            {
                orders = _context.Order.Where(o => o.BeginDate >= model.BeginDate).ToList();
                if (!string.IsNullOrEmpty(model.UserId))
                {
                    List<Order> userOrders = new List<Order>();
                    foreach (var order in orders)
                    {
                        if (model.UserId.Contains(order.CorpClientId))
                        {
                            userOrders.Add(order);
                        }
                    }

                    orders = userOrders;
                }
                else if (model.ConfirmedOrders && !model.DeclinedOrders)
                {
                    orders = orders.Where(o => o.Status.Contains("одобрена")).ToList();
                }
                else if (model.DeclinedOrders && !model.ConfirmedOrders)
                {
                    orders = orders.Where(o => o.Status.Contains("отклонена")).ToList();
                }
                else if (model.ConfirmedOrders && model.DeclinedOrders)
                {
                    orders = orders.OrderByDescending(o => o.BeginDate).ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            string reportLink = _reportGenerator.GetReportLink(orders);
            if (string.IsNullOrEmpty(reportLink))
            {
                return RedirectToAction("MarketOperatorProfile", "Manage", "_Reports");
            }
            return Redirect(reportLink);
        }
    }
}
