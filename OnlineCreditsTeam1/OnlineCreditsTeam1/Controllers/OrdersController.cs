﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OnlineCreditsTeam1.Data;
using OnlineCreditsTeam1.Extensions;
using OnlineCreditsTeam1.Models;
using OnlineCreditsTeam1.Models.DocumentsViewModels;
using OnlineCreditsTeam1.Models.OrderViewModel;
using OnlineCreditsTeam1.Services;

namespace OnlineCreditsTeam1.Controllers
{
    [Produces("application/json")]
    
    public class OrdersController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SmsSender _smsSender;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly PasswordGenerator _passwordGenerator;
        private readonly IStringLocalizer<OrdersController> _localizer;
        private readonly ILogger _logger;
        private readonly DocumentLoader _documentLoader;
        private readonly UserActionLogger _actionLogger;
        private readonly OrderChecker _orderChecker;
        private readonly FileEraser _fileEraser;
        private readonly JsonService _jsonService;
        private readonly UserLogWriter _userLogWriter;

        public OrdersController(ApplicationDbContext context,
                                UserLogWriter userLogWriter,
                                SmsSender smsSender,
                                JsonService jsonService,
                                FileEraser fileEraser,
                                OrderChecker orderChecker,
                                UserManager<ApplicationUser> userManager,
                                PasswordGenerator passwordGenerator,
                                UserActionLogger actionLogger,
                                DocumentLoader documentLoader,
                                IStringLocalizer<OrdersController> localizer)
        {
            _documentLoader = documentLoader;
            _userLogWriter = userLogWriter;
            _smsSender = smsSender;
            _jsonService = jsonService;
            _fileEraser = fileEraser;
            _orderChecker = orderChecker;
            _passwordGenerator = passwordGenerator;
            _userManager = userManager;
            _context = context;
            _actionLogger = actionLogger;
            _localizer = localizer;
        }

        [Route("api/Orders")]
        [HttpGet("[controller]/[action]")]
        public async Task<IActionResult> GraphicRepaimentsDetails(int orderId)
        {
            
            var order = _context.Order.FirstOrDefaultAsync(n => n.Id == orderId).Result;

            List<DateTime> datesOfRepaiments = new List<DateTime>();
            List<decimal> sumsOfRepaiments = new List<decimal>();

            DateTime bufDate = order.BeginDate;
            decimal bufSum = order.Sum / order.Period;
            for (int i = 0; i <= order.Period; i++)
            {
                datesOfRepaiments.Add(bufDate);
                sumsOfRepaiments.Add(bufSum);

                bufDate = bufDate.AddMonths(1);

                bufSum++;
                
            }

            RepaimentGraphicViewModel model = new RepaimentGraphicViewModel()
            {
                StartRepaimentDate = order.BeginDate,
                FinishRepaimentDate = order.FinishDate,
                SumsOfCreditForPeriod = sumsOfRepaiments,
                PeriodsOfRepaiment = datesOfRepaiments,
                SumOfCredit = order.Sum
            };

            return PartialView("_RepaimentGraphicPartial", model);
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> DocumentsDetails(int orderId)
        {
            
            var documents = _context.Documents.Where(n => n.OrderId == orderId).ToList();

            DocumentUploadViewModel model = new DocumentUploadViewModel()
            {
                Documents = documents,
                OrderId = orderId
            };
            return PartialView("_DocumentsPartial", model);
        }

        public async Task<IActionResult> GetLogsByUserId(string userId)
        {
           List<LogModel> logModel = _userLogWriter.GetAllLogs(userId);
           return PartialView(logModel);
        }

        [Authorize]
        [Route("Orders")]
        [HttpGet("[controller]/[action]")]
        public async Task<IActionResult> ConfirmDocument(string documentId)
        {
            try
            {
                Document document = await _context.Documents.FirstAsync(d => d.Id == documentId);
                document.IsApproved = true;
                LogModel log = new LogModel
                {
                    ActionDate = DateTime.Now,
                    LogMessage = $"Document {document.Title} has confirmed by admin",
                    UserId = document.UserId
                };
                _userLogWriter.WriteLog(log);
                _context.Documents.Update(document);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return RedirectToAction("BankOperatorProfile", "Manage", "_Orders");
        }

        [Authorize]
        [Route("Orders")]
        [HttpPost("[controller]/[action]")]
        public async Task<IActionResult> DeclineDocument(string documentId, string message)
        {
            try
            {
                Document document = await _context.Documents.FirstAsync(d => d.Id == documentId);
                document.IsApproved = false;
                document.DeclineMessage = message;
                LogModel log = new LogModel
                {
                    ActionDate = DateTime.Now,
                    LogMessage = $"Document {document.Title} has declined by admin on {message}",
                    UserId = document.UserId
                };
                _userLogWriter.WriteLog(log);
                _context.Documents.Update(document);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return RedirectToAction("BankOperatorProfile", "Manage", "_Orders");
        }

        [Route("Orders")]
        [HttpPost("[controller]/[action]")]
        public async Task<IActionResult> DeleteDocument(string documentId)
        {
            
            try
            {
                Document document =await _context.Documents.FirstAsync(doc => doc.Id == documentId);
                _fileEraser.DeleteFile(document.Path);
                LogModel log = new LogModel
                {
                    ActionDate = DateTime.Now,
                    LogMessage = $"Document {document.Title} has been deleted by user",
                    UserId = document.UserId
                };
                _userLogWriter.WriteLog(log);
                _context.Documents.Remove(document);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return RedirectToAction( "UserProfile", "Manage", "_Orders");
        }

        public bool IsOrderConfirmed(OrderConfirmation orderConfirmation)
        {
            var result = false;
            if (User.IsInRole("user"))
            {
                result = orderConfirmation.IsUserConfirm;

            }
            else if (User.IsInRole("BankOperator"))
            {
                result = orderConfirmation.IsAdminConfirm;
            }

            return result;
        }

        public void StatusChanger(Order order, bool confirmed)
        {
            if (confirmed)
            {
                order.Status = User.IsInRole("user") ? "Ожидает проверки" : "Подтверждено банком";
            }
            else
            {
                order.Status = User.IsInRole("user") ? "Отклонено клиентом" : "Отклонено банком";
            }
            LogModel log = new LogModel
            {
                ActionDate = DateTime.Now,
                LogMessage = $"Order status has been changed to {order.Status}",
                UserId = order.ClientId
            };
            _userLogWriter.WriteLog(log);
            string message = $"Статус заявки изменился на {order.Status}";
            _smsSender.SendSms(message, _smsSender.GetUserNumberById(order.ClientId));

        }

        [Route("Orders")]
        [HttpGet("[controller]/[action]")]
        public async Task<IActionResult> ConfirmOrder(int orderId)
        {
            OrderConfirmation orderConfirmation = new OrderConfirmation();
            Order order = await _context.Order.FirstAsync(o => o.Id == orderId);

            if (_orderChecker.CheckOrderConfirmationEntity(orderId) == false)
            {
                orderConfirmation = _orderChecker.CreateOrderConfirmationEntity(orderId);
            }
            else
            {
                orderConfirmation = await _context.OrderConfirmations.FirstAsync(o => o.OrderId == orderId);
            }

            if (User.IsInRole("user"))
            {
                orderConfirmation.IsUserConfirm = true;
            }
            else if (User.IsInRole("BankOperator"))
            {
                orderConfirmation.IsAdminConfirm = true;
            }
            StatusChanger(order, true);
            _context.UpdateRange(orderConfirmation, order);
            _context.SaveChanges();
            return RedirectToAction(User.IsInRole("user") ? "UserProfile" : "BankOperatorProfile", "Manage", "_Orders");
        }

        [Route("Orders")]
        [HttpGet("[controller]/[action]")]
        public async Task<IActionResult> DeclineOrder(int orderId)
        {
            OrderConfirmation orderConfirmation = new OrderConfirmation();
            Order order = await _context.Order.FirstAsync(o => o.Id == orderId);
            if (_orderChecker.CheckOrderConfirmationEntity(orderId) == false)
            {
                orderConfirmation = _orderChecker.CreateOrderConfirmationEntity(orderId);
            }
            else
            {
                orderConfirmation = await _context.OrderConfirmations.FirstAsync(o => o.OrderId == orderId);
            }
            if (User.IsInRole("user"))
            {
                orderConfirmation.IsUserConfirm = false;
                orderConfirmation.UserDeclineMessage = "Передумал";
               }
            else if (User.IsInRole("BankOperator"))
            {
                orderConfirmation.IsAdminConfirm = false;
                orderConfirmation.AdminDeclineMessage = "Отклонена и все тут";
            }
            StatusChanger(order, false);
            LogModel log = new LogModel
            {
                ActionDate = DateTime.Now,
                LogMessage = $"User {_userManager.GetUserAsync(User).Result.Role} left decline message by order {orderConfirmation.UserDeclineMessage}",
                UserId = order.ClientId
            };
            _userLogWriter.WriteLog(log);
            _context.UpdateRange(orderConfirmation, order);
            _context.SaveChanges();
            return RedirectToAction(User.IsInRole("user") ? "UserProfile" : "BankOperatorProfile", "Manage", "_Orders");
        }

        [HttpPost("[controller]/[action]")]
        public async Task<IActionResult> DocumentScanLoad(DocumentUploadViewModel model)
        {
            SettingsService service = new SettingsService();
            long size = (long)service.GetSettnigs().FileSize;
            try
            {
                Document doc = _documentLoader.UploadScan(model);
                if (doc == null)
                {                   
                    return View(model);
                }
                model.Json = _jsonService.CreateJson(model);
                if (model.Documents != null)
                {
                    model.Documents.Add(doc);
                }
                else
                {
                    model.Documents = new List<Document>();
                    model.Documents.Add(doc);
                }

                model.OrderId = doc.OrderId;
                LogViewModel logModel = new LogViewModel { UserId = model.UserId, Action = $"Загрузка документа {model.DocumentTitle}", ActionDate = DateTime.Now };
                _actionLogger.SaveActionInLog(logModel);
                LogModel log = new LogModel
                {
                    ActionDate = DateTime.Now,
                    LogMessage = $"Document {model.DocumentTitle} scan has been uploaded by user",
                    UserId = model.UserId
                };
                _userLogWriter.WriteLog(log);
                ViewData["Message"] = "Success. Документ успешно загружен";
            }
            catch (Exception e)
            {
                ViewData["Message"] = "Error. Документ не загружен";
            }
            return RedirectToAction(User.IsInRole("user") ? "UserProfile" : "BankOperatorProfile", "Manage", "_Orders");
        }

        // POST: api/Orders
        [Route("api/Orders")]
        [HttpPost]
        public async Task<string> PostOrder([FromBody] CreateOrderViewModel model)
        {
            SettingsService settingsService = new SettingsService();
            SmsSender smsSender = new SmsSender(_context);
            string message = _localizer["application_filed"].Value;
            try
            {
                if (ModelState.IsValid)
                {
                    decimal UltimateSum = 0;
                    double PercentRateDown = 0;
                    Limit globalLimit = null;
                    string password = _passwordGenerator.GeneratePassword(6);
                    var user =await _context.Users.FirstOrDefaultAsync(n => n.UserName == model.PhoneNumber);
                    Limit limit = null;
                    ApplicationUser market = await _context.Users.FirstOrDefaultAsync(m => m.Id == model.MarketId);
                    if (user != null)
                    {
                        //Если после одобрения заявки у нее будет другой статус а не Погашен, то нужно будет изменить
                    var repeatOrder = await _context.Order.FirstOrDefaultAsync(n =>
                        n.Status == "Погашен" && n.ClientId == user.Id && n.CorpClientId == market.Id);
                    if (repeatOrder != null)
                    {
                        var preferentialInterestRateList =
                            await _context.PreferentialRates.Where(m => 
                            m.CorpClientId == market.Id && m.BeginDate<=DateTime.Now && m.FinishDate > DateTime.Now &&
                            m.TotalSumFrom<=model.Sum && m.TotalSumTo >= model.Sum && m.CreditTermFrom <= model.Period &&
                            m.CreditTermTo >= model.Period).OrderByDescending(l => l.BeginDate).ToListAsync();
                        if (preferentialInterestRateList.Count > 0)
                        {
                           var preferentialInterestRate = preferentialInterestRateList[0];
                           PercentRateDown = preferentialInterestRate.PercentRateDown;
                           UltimateSum = model.Sum / 100 * (decimal)PercentRateDown + model.Sum;
                        }
                        else
                        {
                            message = "Извините, у магазина нет такой льготной ставки";
                            return message;
                        }
                    }
                    else
                    {
                        var InterestRateList =
                            await _context.InterestRates.Where(m =>
                                m.CorpClientId == market.Id && m.BeginDate <= DateTime.Now && m.FinishDate > DateTime.Now &&
                                m.TotalSumFrom <= model.Sum && m.TotalSumTo >= model.Sum && m.CreditTermFrom <= model.Period &&
                                m.CreditTermTo >= model.Period).OrderByDescending(l => l.BeginDate).ToListAsync();
                        if (InterestRateList.Count > 0)
                        {
                            var InterestRate = InterestRateList[0];
                            PercentRateDown = InterestRate.PercentRate;
                            UltimateSum = model.Sum / 100 * (decimal)PercentRateDown + model.Sum;

                        }
                        else
                        {
                            message = "Извините, у магазина нет такой процентной ставки";
                            return message;
                        }
                    }
                    }
                    if (market != null)
                    {
                        if (market.Status == "Заблокирован")
                        {
                            message = _localizer["market_locked_message"].Value;
                            return message;
                        }
                        else
                        {
                            var limits = await _context.Limit.Where(l => l.UserId == market.Id &&
                                                                     l.isActive &&
                                                                     l.CustomLimitFinishDate > DateTime.Now &&
                                                                     l.CustomLimitBeginDate <= DateTime.Now)
                            .OrderByDescending(l => l.CustomLimitBeginDate).ToListAsync();
                            if (limits.Count > 0)
                            {
                                limit = limits[0];
                            }

                            limits = await _context.Limit.Where(l => l.UserId == null &&
                                                                  l.isActive &&
                                                                  l.CustomLimitFinishDate > DateTime.Now &&
                                                                  l.CustomLimitBeginDate <= DateTime.Now)
                                    .OrderByDescending(l => l.CustomLimitBeginDate).ToListAsync();
                            if (limits.Count > 0)
                            {
                                globalLimit = limits[0];
                            }

                            if (limit == null)
                            {
                                limit = globalLimit;
                            }

                            if (limit.LimitNumberCounter >= limit.MaxOrdersOnPeriod ||
                                limit.LimitSumCounter + model.Sum > limit.MaxSumOnPeriod)
                            {
                                message = _localizer["market_application_deceived"].Value;
                                return message;
                            }

                            DateTime finalDate = DateTime.Now.AddMonths((int)limit.MaxCreditSprint);
                            if (DateTime.Parse(model.DesiredDay) > finalDate)
                            {
                                message = _localizer["date_application_deceived"].Value;
                                return message;
                            }
                        }

                        if (user == null)
                        {
                            var InterestRateList =
                                await _context.InterestRates.Where(m =>
                                    m.CorpClientId == market.Id && m.BeginDate <= DateTime.Now && m.FinishDate > DateTime.Now &&
                                    m.TotalSumFrom <= model.Sum && m.TotalSumTo >= model.Sum && m.CreditTermFrom <= model.Period &&
                                    m.CreditTermTo >= model.Period).OrderByDescending(l => l.BeginDate).ToListAsync();
                            if (InterestRateList.Count > 0)
                            {
                                var InterestRate = InterestRateList[0];
                                PercentRateDown = InterestRate.PercentRate;
                                UltimateSum = model.Sum / 100 * (decimal) PercentRateDown + model.Sum;
                            }
                            else
                            {
                                message = "Извините, у магазина нет такой процентной ставки";
                                return message;
                            }

                            user = new ApplicationUser
                            {
                                Role = "user",
                                UserName = model.PhoneNumber,
                                PhoneNumber = model.PhoneNumber,
                                Email = model.Email,
                                CreateTime = DateTime.Now,
                                PhotoPath = "/StaticData/Anonymous/notimageuser.jpg",
                                Name = $"{model.Name} {model.LastName} {model.FatherName}",
                                CreditAdminId = market.CreditAdminId,
                                Status = "Активен"
                            };
                            
                            IdentityResult result = await _userManager.CreateAsync(user, password);
                            if (result.Succeeded)
                            {
                                await _userManager.AddToRoleAsync(user, "user");
                            }

                            message = _localizer["application_filed_account_create"].Value;

                        }

                        if (user.Status == "Активен")
                        {
                            if (limit == globalLimit)
                            {
                                limit.LimitNumberCounter += 1;
                                limit.LimitSumCounter += model.Sum;
                            }
                            else
                            {
                                limit.LimitNumberCounter += 1;
                                limit.LimitSumCounter += model.Sum;
                                globalLimit.LimitNumberCounter += 1;
                                globalLimit.LimitSumCounter += model.Sum;
                            }

                            
                            Order order = new Order();
                            order.BeginDate = DateTime.Now;
                            order.FinishDate = DateTime.Now.AddMonths(model.Period);
                            order.Period = model.Period;
                            order.ClientId = user.Id;
                            order.CorpClientId = model.MarketId;
                            order.Status = "new";
                            order.Sum = model.Sum;
                            order.DesiredDay = DateTime.Parse(model.DesiredDay);
                            order.PercentRateDown = PercentRateDown;
                            order.UltimateSum = UltimateSum;

                            if (string.IsNullOrEmpty(model.OrderInfoJson))
                            {
                                OrderInfo orderInfo = new OrderInfo
                                {
                                    Name = "Demo name chapelnik",
                                    Price = 999.99,
                                    Currency = "KGS"
                                };
                                List<OrderInfo> ordersInfo = new List<OrderInfo>();
                                ordersInfo.Add(orderInfo);
                                order.OrderInfoJson = JsonConvert.SerializeObject(ordersInfo);
                            }
                            else
                            {
                                order.OrderInfoJson = model.OrderInfoJson;
                            }
                            if (model.Sum <= settingsService.GetSettnigs().SumAutoAcceptOrder)
                            {
                                order.Status = "Одобрена";
                                smsSender.SendSms("Ваша заявка автоматически одобрена", user.UserName);
                            }
                            _context.Add(order);

                            await _context.SaveChangesAsync();
                        }
                        else
                        {
                            message = "Данный пользователь заблокирован и не может осуществлять заказы";
                            return message;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                message = "ошибка";
            }

            return message;
        }

        
    }
}