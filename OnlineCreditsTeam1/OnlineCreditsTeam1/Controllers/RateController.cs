﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using OnlineCreditsTeam1.Data;
using OnlineCreditsTeam1.Models;
using OnlineCreditsTeam1.Models.ManageViewModels;

namespace OnlineCreditsTeam1.Controllers
{
    [Authorize(Roles = "admin")]
    public class RateController : Controller
    {
        private readonly ApplicationDbContext _context;

        public RateController(ApplicationDbContext context)
        {
            _context = context; 
        }
       
        public async Task<IActionResult> CreateStandart(UserProfileViewModel model)
        {
            var userList = await _context.Users.Where(u => u.Role == "MarketOperator" || u.Role == "BankOperator")
                .ToListAsync();
            ViewBag.UserList = userList;
            if (model.InterestRate.BeginDate < DateTime.Today)
            {
                string message = "Ошибка! Не корректно введенные данные";
                string errorMessage = "Дата начала действия кредитной ставки не может быть раньше текущей даты";
                ViewBag.MessageError = errorMessage;
                ViewBag.message = message;
                return View(model);
            }
            if (model.InterestRate.FinishDate <= model.InterestRate.BeginDate)
            {
                string message = "Ошибка! Не корректно введенные данные";
                string errorMessage = "Дата окончания действия процентной ставки не может быть раньше даты начала её действия + 1 день ";
                ViewBag.MessageError = errorMessage;
                ViewBag.message = message;
                return View(model);
            }
            if (model.InterestRate.CreditTermFrom == null) model.InterestRate.CreditTermFrom = 0;
            if (model.InterestRate.CreditTermTo == null) model.InterestRate.CreditTermTo = 24;
            if (model.InterestRate.CreditTermTo <= model.InterestRate.CreditTermFrom)
            {
                string message = "Ошибка! Не корректно введенные данные";
                string errorMessage = "Минимальный срок кредита не может быть больше максимального срока кредита";
                ViewBag.MessageError = errorMessage;
                ViewBag.message = message;
                return View(model);
            }
            if (model.InterestRate.TotalSumFrom == null) model.InterestRate.TotalSumFrom = 0;
            if (model.InterestRate.TotalSumTo == null) model.InterestRate.TotalSumTo = 9999999;
            if (model.InterestRate.TotalSumTo <= model.InterestRate.TotalSumFrom)
            {
                string message = "Ошибка! Не корректно введенные данные";
                string errorMessage = "Минимальная сумма кредита не может быть больше максимальной суммы кредита";
                ViewBag.MessageError = errorMessage;
                ViewBag.message = message;
                return View(model);
            }

            if (!ModelState.IsValid) return RedirectToAction("AdminProfile", "Manage");
            InterestRate rate = new InterestRate
            {
                PercentRate = model.InterestRate.PercentRate,
                BeginDate = model.InterestRate.BeginDate,
                FinishDate = model.InterestRate.FinishDate,
                CorpClientId = model.InterestRate.CorpClientId,
                CreditTermFrom = model.InterestRate.CreditTermFrom,
                CreditTermTo = model.InterestRate.CreditTermTo,
                CurrencyCode = model.InterestRate.CurrencyCode,
                TotalSumFrom = model.InterestRate.TotalSumFrom,
                TotalSumTo = model.InterestRate.TotalSumTo
            };
            _context.InterestRates.Add(rate);
            _context.SaveChanges();

            return RedirectToAction("AdminProfile", "Manage", "_Rates");
        }

        public async Task<IActionResult> CreateLow(UserProfileViewModel model)
        {
            var userList = await _context.Users.Where(u => u.Role == "MarketOperator" || u.Role == "BankOperator")
                .ToListAsync();
            ViewBag.UserList = userList;
            if (model.PreferentialInterestRate.BeginDate < DateTime.Today)
            {
                string message = "Ошибка! Не корректно введенные данные";
                string errorMessage = "Дата начала действия кредитной ставки не может быть раньше текущей даты";
                ViewBag.MessageError = errorMessage;
                ViewBag.message = message;
                return View(model);
            }
            if (model.PreferentialInterestRate.FinishDate <= model.PreferentialInterestRate.BeginDate)
            {
                string message = "Ошибка! Не корректно введенные данные";
                string errorMessage = "Дата окончания действия процентной ставки не может быть раньше даты начала её действия + 1 день ";
                ViewBag.MessageError = errorMessage;
                ViewBag.message = message;
                return View(model);
            }
            if (model.PreferentialInterestRate.CreditTermFrom == null) model.PreferentialInterestRate.CreditTermFrom = 0;
            if (model.PreferentialInterestRate.CreditTermTo == null) model.PreferentialInterestRate.CreditTermTo = 24;
            if (model.PreferentialInterestRate.CreditTermTo <= model.PreferentialInterestRate.CreditTermFrom)
            {
                string message = "Ошибка! Не корректно введенные данные";
                string errorMessage = "Минимальный срок кредита не может быть больше максимального срока кредита";
                ViewBag.MessageError = errorMessage;
                ViewBag.message = message;
                return View(model);
            }
            if (model.PreferentialInterestRate.TotalSumFrom == null) model.PreferentialInterestRate.TotalSumFrom = 0;
            if (model.PreferentialInterestRate.TotalSumTo == null) model.PreferentialInterestRate.TotalSumTo = 9999999;
            if (model.PreferentialInterestRate.TotalSumTo <= model.PreferentialInterestRate.TotalSumFrom)
            {
                string message = "Ошибка! Не корректно введенные данные";
                string errorMessage = "Минимальная сумма кредита не может быть больше максимальной суммы кредита";
                ViewBag.MessageError = errorMessage;
                ViewBag.message = message;
                return View(model);
            }

            if (!ModelState.IsValid) return RedirectToAction("AdminProfile", "Manage");
            PreferentialInterestRate rate = new PreferentialInterestRate
            {
                PercentRateDown = model.PreferentialInterestRate.PercentRateDown,
                BeginDate = model.PreferentialInterestRate.BeginDate,
                FinishDate = model.PreferentialInterestRate.FinishDate,
                CorpClientId = model.PreferentialInterestRate.CorpClientId,
                CreditTermFrom = model.PreferentialInterestRate.CreditTermFrom,
                CreditTermTo = model.PreferentialInterestRate.CreditTermTo,
                CurrencyCode = model.PreferentialInterestRate.CurrencyCode,
                TotalSumFrom = model.PreferentialInterestRate.TotalSumFrom,
                TotalSumTo = model.PreferentialInterestRate.TotalSumTo
            };
            _context.PreferentialRates.Add(rate);
            _context.SaveChanges();

            return RedirectToAction("AdminProfile", "Manage", "_Rates");
        }

        [HttpGet]
        public async Task<IActionResult> GetRates()
        {
            List<InterestRate> rates = new List<InterestRate>();
            try
            {
                rates = _context.InterestRates.Include(r => r.CorpClient).OrderBy(r => r.BeginDate).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                InterestRate rate = new InterestRate
                {
                    Id = 0,
                    BeginDate = DateTime.Now,
                    FinishDate = DateTime.MaxValue,
                    CreditTermFrom = 1,
                    CreditTermTo = 12,
                    CorpClientId = "Demo client info",
                    CurrencyCode = 999,
                    TotalSumFrom = 0,
                    TotalSumTo = 999999,
                    PercentRate = 5
                };

                rates.Add(rate);
            }
            return PartialView("PercentRates", rates);
        }

        [HttpGet]
        public async Task<IActionResult> GetLowerRates()
        {
            List<PreferentialInterestRate> rates = new List<PreferentialInterestRate>();
            try
            {
                rates = _context.PreferentialRates.Include(r => r.CorpClient).OrderBy(r => r.BeginDate).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                PreferentialInterestRate rate = new PreferentialInterestRate
                {
                    Id = 0,
                    BeginDate = DateTime.Now,
                    FinishDate = DateTime.MaxValue,
                    CreditTermFrom = 1,
                    CreditTermTo = 12,
                    CorpClientId = "Demo client info",
                    CurrencyCode = 999,
                    TotalSumFrom = 0,
                    TotalSumTo = 999999,
                    PercentRateDown = 0.5
                };
                
                rates.Add(rate);
            }
            return PartialView("LowerRates", rates);
        }

        public IActionResult DeleteStandartRate(int id)
        {
            try
            {
                InterestRate rate = _context.InterestRates.First(r => r.Id == id);
                _context.Remove(rate);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return RedirectToAction("AdminProfile", "Manage", "_Rates");
        }

        public IActionResult DeleteLowerRate(int id)
        {
            try
            {
                PreferentialInterestRate rate = _context.PreferentialRates.First(r => r.Id == id);
                _context.Remove(rate);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return RedirectToAction("AdminProfile", "Manage", "_Rates");
        }

        [HttpGet]
        public async Task<IActionResult> EditStandartRate(int id)
        {
            var userList = await _context.Users.Where(u => u.Role == "MarketOperator" || u.Role == "BankOperator")
                .ToListAsync();
            ViewBag.UserList = userList;
            return View(_context.InterestRates.First(r => r.Id == id));
        }

        [HttpGet]
        public async Task<IActionResult> EditLowerRate(int id)
        {
            var userList = await _context.Users.Where(u => u.Role == "MarketOperator" || u.Role == "BankOperator")
                .ToListAsync();
            ViewBag.UserList = userList;

            return View(_context.PreferentialRates.First(r => r.Id == id));
        }


        public async Task<IActionResult> EditStandartRate(InterestRate model)
        {
            var userList = await _context.Users.Where(u => u.Role == "MarketOperator" || u.Role == "BankOperator")
                .ToListAsync();
            ViewBag.UserList = userList;
            if (model.BeginDate < DateTime.Today)
            {
                string message = "Ошибка! Не корректно введенные данные";
                string errorMessage = "Дата начала действия кредитной ставки не может быть раньше текущей даты";
                ViewBag.MessageError = errorMessage;
                ViewBag.message = message;
                return View(model);
            }
            if (model.FinishDate <= model.BeginDate)
            {
                string message = "Ошибка! Не корректно введенные данные";
                string errorMessage = "Дата окончания действия процентной ставки не может быть раньше даты начала её действия + 1 день ";
                ViewBag.MessageError = errorMessage;
                ViewBag.message = message;
                return View(model);
            }
            if (model.CreditTermFrom == null) model.CreditTermFrom = 0;
            if (model.CreditTermTo == null) model.CreditTermTo = 24;
            if (model.TotalSumFrom == null) model.TotalSumFrom = 0;
            if (model.TotalSumTo == null) model.TotalSumTo = 9999999;
            _context.Update(model);
            _context.SaveChanges();
            return RedirectToAction("AdminProfile", "Manage", "_Rates");
        }
        public async Task<IActionResult> EditLowerRate(PreferentialInterestRate model)
        {
            var userList = await _context.Users.Where(u => u.Role == "MarketOperator" || u.Role == "BankOperator")
                .ToListAsync();
            ViewBag.UserList = userList;
            if (model.BeginDate < DateTime.Today)
            {
                string message = "Ошибка! Не корректно введенные данные";
                string errorMessage = "Дата начала действия кредитной ставки не может быть раньше текущей даты";
                ViewBag.MessageError = errorMessage;
                ViewBag.message = message;
                return View(model);
            }
            if (model.FinishDate <= model.BeginDate)
            {
                string message = "Ошибка! Не корректно введенные данные";
                string errorMessage = "Дата окончания действия процентной ставки не может быть раньше даты начала её действия + 1 день ";
                ViewBag.MessageError = errorMessage;
                ViewBag.message = message;
                return View(model);
            }
            if (model.CreditTermFrom == null) model.CreditTermFrom = 0;
            if (model.CreditTermTo == null) model.CreditTermTo = 24;
            if (model.TotalSumFrom == null) model.TotalSumFrom = 0;
            if (model.TotalSumTo == null) model.TotalSumTo = 9999999;
            _context.Update(model);
            _context.SaveChanges();
            return RedirectToAction("AdminProfile", "Manage", "_Rates");
        }
    }
}