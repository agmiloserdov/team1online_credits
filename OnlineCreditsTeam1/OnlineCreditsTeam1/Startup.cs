﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OnlineCreditsTeam1.Models;
using OnlineCreditsTeam1.Services;
using OnlineCreditsTeam1.Data;

namespace OnlineCreditsTeam1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddCors();
            
            // Add application services.
            services.AddSingleton<FileUploadService>();
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddSingleton<PasswordGenerator>();
            services.AddSingleton<SmsSender>();
            services.AddTransient<UserActionLogger>();
            services.AddTransient<DocumentLoader>();
            services.AddTransient<JsonService>();
            services.AddTransient<OrderChecker>();
            services.AddTransient<FileEraser>();
            services.AddTransient<SmsSender>();
            services.AddTransient<UserLogWriter>();
            services.AddTransient<ReportGenerator>();
            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 6;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Password.RequiredUniqueChars = 0;

                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;
                options.Lockout.AllowedForNewUsers = true;

                // User settings
                options.User.RequireUniqueEmail = false;

            });
            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.AddMvc().AddDataAnnotationsLocalization().AddViewLocalization();
            services.AddTransient<IRepository, UserLogRepository>();
            services.AddTransient<ILogRepository, ActionLogRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            env.EnvironmentName = EnvironmentName.Production;
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseStatusCodePagesWithRedirects("/{0}.html");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            
            var supportedCultures = new[]

            {
                new CultureInfo("ru"),

                new CultureInfo("en"),

                new CultureInfo("de")

            };
            app.UseRequestLocalization(new RequestLocalizationOptions

            {

                DefaultRequestCulture = new RequestCulture("ru"),

                SupportedCultures = supportedCultures,

                SupportedUICultures = supportedCultures

            });


            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyHeader().WithMethods("POST"));
            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyHeader().WithMethods("POST"));
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template:"{controller=Account}/{action=CheckUser}/{id?}");
            });

           
        }
    }
}
