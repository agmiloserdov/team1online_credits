﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using OnlineCreditsTeam1.Models;

namespace OnlineCreditsTeam1.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public DbSet<Document> Documents { get; set; }
        public DbSet<UserActionLogModel> ActionLogs { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<OnlineCreditsTeam1.Models.Limit> Limit { get; set; }
        public DbSet<OrderConfirmation> OrderConfirmations { get; set; }
        public DbSet<InterestRate> InterestRates { get; set; }
        public DbSet<PreferentialInterestRate> PreferentialRates { get; set; }
        public DbSet<MicroCredit> MicroCredits { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Provider> Providers { get; set; }
    }
}
