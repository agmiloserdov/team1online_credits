﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OnlineCreditsTeam1.Models;

namespace OnlineCreditsTeam1.Data
{
    public interface ILogRepository
    {
        void Add(UserActionLogModel logModel);
        void SaveChanges();
        List<UserActionLogModel> GetLogsById(string userId);
        List<UserActionLogModel> GetAllLogs();

    }
}
