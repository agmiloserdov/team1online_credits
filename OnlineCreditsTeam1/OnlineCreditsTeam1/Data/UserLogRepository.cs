﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OnlineCreditsTeam1.Models;
using OnlineCreditsTeam1.Services;

namespace OnlineCreditsTeam1.Data
{
    public class UserLogRepository : IRepository
    {
        private readonly ApplicationDbContext _context;

        public UserLogRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public void Update(ApplicationUser user)
        {
            _context.Users.Update(user);
        }

        public ApplicationUser GetUserById(string userId)
        {
            return _context.Users.FirstOrDefault(u => u.Id == userId);
        }
    }
}
