﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OnlineCreditsTeam1.Models;

namespace OnlineCreditsTeam1.Data
{
    public class ActionLogRepository : ILogRepository
    {
        private readonly ApplicationDbContext _context;

        public ActionLogRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Add(UserActionLogModel logModel)
        {
            _context.Add(logModel);
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public List<UserActionLogModel> GetLogsById(string userId)
        {
            return _context.ActionLogs.Where(l => l.UserId == userId)
                .OrderByDescending(log => log.ActionDate).ToList();
        }

        public List<UserActionLogModel> GetAllLogs()
        {
           return _context.ActionLogs.OrderByDescending(log => log.ActionDate).ToList();
        }
    }
}
